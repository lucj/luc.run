---
title: ''
weight: 6
---

The **Troubleshooting** represents about 30% of the exam. The topics to study include:

- Evaluate cluster and node logging
- Understand how to monitor applications
- Manage container stdout & stderr logs
- Troubleshoot application failure
- Troubleshoot cluster component failure
- Troubleshoot networking