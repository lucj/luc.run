---
title: ''
weight: 5
---

The **Storage** represents about 10% of the exam. The topics to study include:

- Understand storage classes, persistent volumes
- Understand volume mode, access modes and reclaim policies for volumes
- Understand persistent volume claims primitive
- Know how to configure applications with persistent storage