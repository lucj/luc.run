---
title: ''
weight: 1
---

The **Cluster Architecture, Installation & Configuration** represents about 25% of the exam. The topics to study include:

- Manage role based access control (RBAC)
- Use Kubeadm to install a basic cluster
- Manage a highly-available Kubernetes cluster
- Provision underlying infrastructure to deploy a Kubernetes cluster
- Perform a version upgrade on a Kubernetes cluster using Kubeadm
- Implement etcd backup and restore