---
title: 'Preparing the CKA'
weight: 4
---

![CKA](../images/logo_cka.png)

This section contains many exercises that will help to prepare for the CKA (Certified Kubernetes Administrator) certification

{{< callout type="info" >}}
To complete the exercises, it is recommended to [create a kubeadm cluster](./Cluster/Creation-Multipass/) first. This setup uses [https://multipass.run](https://multipass.run) a tool that simplifies the creation of local Ubuntu virtual machines.
{{< /callout >}}
