# Déploiement d'un site Wordpress

Dans cette partie, nous allons utiliser Docker Compose afin de déployer le moteur wordpress et la database mysql que celui-ci utilise. 

## Le fichier compose.yml

Créez un répertoire wp et à l'intérieur de celui-ci créez le fichier compose.yml avec le contenu suivant:

```yaml {filename="compose.yml"}
services:
  wordpress:
    image: wordpress:4.8
    restart: always
    ports:
      - 8080:80
    environment:
      WORDPRESS_DB_PASSWORD: example
  mysql:
    image: mysql:5.7
    restart: always
    environment:
      MYSQL_ROOT_PASSWORD: example
```

Ce fichier défini 2 services:
* la base de données basée sur l'image officielle mysql dans la version 5.7
* Le mot de passe à utiliser est définit dans une variable d'environnement 

le moteur wordpress basé sur l'image officielle wordpress dans la version 4.8
Le mot de passe de la base de donnée est spécifié de façon à ce que le moteur wordpress puisse s'y connecter. De plus, on spécifie le port sur lequel l'interface web sera disponible sur la machine hôte. 

Note: le mot de passe est ici donné dans une variable d'environnement, c'est acceptable dans un contexte de développement mais il faudra procéder autrement dans un contexte de production afin de protéger les informations confidentielles. 

Pour chaque service, nous utilisons l'instruction restart: always de façon à ce que le container du service soit redémarré en cas d'arrêt non prévu.

## Lancement de l'application

Lancez les différents services de l'application avec la commande suivante:

```bash
$ docker compose up -d
[+] Running 31/31
 ✔ wordpress Pulled                                                                                                                                       16.6s
   ✔ 85b1f47fba49 Pull complete                                                                                                                            8.5s
   ✔ d8204bc92725 Pull complete                                                                                                                           12.7s
   ✔ 92fc16bb18e4 Pull complete                                                                                                                           12.7s
   ✔ 31098e61b2ae Pull complete                                                                                                                           13.0s
   ✔ f6ae64bfd33d Pull complete                                                                                                                           13.1s
   ✔ 003c1818b354 Pull complete                                                                                                                           13.1s
   ✔ a6fd4aeb32ad Pull complete                                                                                                                           13.1s
   ✔ a094df7cedc1 Pull complete                                                                                                                           13.1s
   ✔ e3bf6fc1a51d Pull complete                                                                                                                           13.3s
   ✔ ad235c260360 Pull complete                                                                                                                           13.3s
   ✔ edbf48bcbd7e Pull complete                                                                                                                           13.9s
   ✔ fd6ae81d5745 Pull complete                                                                                                                           13.9s
   ✔ 69838fd876d6 Pull complete                                                                                                                           13.9s
   ✔ 3186ebffd72d Pull complete                                                                                                                           14.1s
   ✔ b24a415ea2c0 Pull complete                                                                                                                           14.1s
   ✔ 225bda14ea90 Pull complete                                                                                                                           14.1s
   ✔ d47a53aaaacc Pull complete                                                                                                                           14.8s
   ✔ ce9b97a033e9 Pull complete                                                                                                                           14.8s
 ✔ mysql Pulled                                                                                                                                           16.7s
   ✔ 20e4dcae4c69 Pull complete                                                                                                                            6.8s
   ✔ 1c56c3d4ce74 Pull complete                                                                                                                            6.9s
   ✔ e9f03a1c24ce Pull complete                                                                                                                            6.9s
   ✔ 68c3898c2015 Pull complete                                                                                                                            7.2s
   ✔ 6b95a940e7b6 Pull complete                                                                                                                            7.3s
   ✔ 90986bb8de6e Pull complete                                                                                                                            7.3s
   ✔ ae71319cb779 Pull complete                                                                                                                            8.4s
   ✔ ffc89e9dfd88 Pull complete                                                                                                                            8.4s
   ✔ 43d05e938198 Pull complete                                                                                                                           15.1s
   ✔ 064b2d298fba Pull complete                                                                                                                           15.1s
   ✔ df9a4d85569b Pull complete                                                                                                                           15.1s
[+] Running 3/3
 ✔ Network wp_default        Created                                                                                                                       0.1s
 ✔ Container wp-wordpress-1  Started                                                                                                                       0.6s
 ✔ Container wp-mysql-1      Started
```

Les 2 images sont récupérées depuis le Docker Hub, et l'application est lancée en background.

## Configuration du site

Comme nous l'avons dit précédemment, l'interface web est disponible sur le port 8080 de la machine hôte, ceci est du à l'instruction suivante effectuant un mapping de ports.

```yaml
    ports:
      - 8080:80
```

Depuis l'interface, on peut donc configurer notre nouveau site wordpress.

![Wordpress](./images/wp1.png)

![Wordpress](./images/wp2.png)

![Wordpress](./images/wp3.png)

Une fois configuré, nous pouvons nous connecter avec le compte que l'on a créé.

![Wordpress](./images/wp4.png)

![Wordpress](./images/wp5.png)

A l'aide des commandes suivantes, on supprime l'application puis on la créé de nouveau.

```bash
docker compose down
docker compose up -d
```

La création sera beaucoup plus rapide que précédemment car les images sont déjà présentes localement. Mais lorsque l'on accède à l'interface web, il nous faudra reconfigurer une nouvelle application, aucun des éléments que nous avions configurés précédemment n'ayant été sauvegardés.

## Persistance des données

L'exemple précédent est très simple et ne prend pas en compte la persistance des données. Afin de sauvegarder ces différents éléments, nous créons 2 volumes:

* db: utilisé pour la sauvegarde des informations du site (comptes des utilisateurs, paramètres de configuration, ...)
* www: utilisé pour la sauvegarde des pages web (thème, plugins, ...)

Nous modifions ensuite le fichier compose.yml afin que les services wordpress et mysql utilisent ces volumes.

```yaml {filename="compose.yml"}
services:
  wordpress:
    image: wordpress:4.8
    restart: always
    ports:
      - 8080:80
    environment:
      WORDPRESS_DB_PASSWORD: example
    volumes:
      - www:/var/www/html
  mysql:
    image: mysql:5.7
    restart: always
    environment:
      MYSQL_ROOT_PASSWORD: example
    volumes:
      - db:/var/lib/mysql
volumes:
  www:
  db:
```

Nous pouvons alors lancer une nouvelle fois notre application et nous pouvons voir que les volumes wp_www et wp_db sont créés, ils sont préfixés par wp le nom du répertoire courant.

```bash
$ docker compose up -d
[+] Running 5/5
 ✔ Network wp_default        Created                                                                                                                       0.1s
 ✔ Volume "wp_db"            Created                                                                                                                       0.0s
 ✔ Volume "wp_www"           Created                                                                                                                       0.0s
 ✔ Container wp-wordpress-1  Started                                                                                                                       0.4s
 ✔ Container wp-mysql-1      Started
```

Après avoir configuré wordpress, les données seront persistées sur la machine hôte et indépendantes du cycle de vie de l'application. Si l'on supprime l'application et que l'on la relance, le site sera toujours disponible, l'ensemble des données ayant été persistées dans les volumes.

```bash
docker compose down
docker compose up -d
```

Note: il est possible de supprimer les volumes en utilisant l'option -v lors que l'on supprime l'application

```bash
[+] Running 5/5
 ✔ Container wp-wordpress-1  Removed                                                                                                                       0.2s
 ✔ Container wp-mysql-1      Removed                                                                                                                       1.3s
 ✔ Volume wp_www             Removed                                                                                                                       0.1s
 ✔ Volume wp_db              Removed                                                                                                                       0.0s
 ✔ Network wp_default        Removed
```
