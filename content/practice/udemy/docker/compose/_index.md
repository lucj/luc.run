---
weight: 4
---

{{< cards >}}
  {{< card link="./elastic" title="Elastic" subtitle="Deployment de la stack Elastic" >}}
  {{< card link="./votingapp" title="VotingApp" subtitle="Deployment de la Voting Application" >}}
  {{< card link="./wordpress" title="Wordpress" subtitle="Deployment de Wordpress" >}}
{{< /cards >}}