---
weight: 8
---

Dans cet exercice, vous allez utiliser le scanner de vulnérabilités *Trivy* pour scanner une image de votre choix.

Vous trouverez davantage d'information sur ce projet dans son repository sur [Github](https://github.com/aquasecurity/trivy)

## Installation

Installez *Trivy* en utilisant la commande suivante:

```bash
curl -sfL https://raw.githubusercontent.com/aquasecurity/trivy/master/contrib/install.sh | sh -s -- -b /usr/local/bin
```

## Scanning d'une image

Lancez simplement Trivy sur une image de votre choix

```bash
trivy image IMAGE_NAME
```

Exemple du scanning de l'image *nginx:1.24*

![Scanning Nginx 1.24](./images/cve-1.png)

Vous obtiendrez alors la liste des vulnérabilités qui ont été détectées dans cette image ainsi qu'un lien qui permet d'obtenir tous les details de chacunes d'entre elles. Par exemple, les information suivante correspondent à la vulnérabilité *CVE-2023-23914* détectée sur l'image *nginx:1.24*.

![CVE-2023-23914](./images/cve-2.png)

Trivy est un outils très utilisé qui fait souvent partie d'une étape dans une pipeline d'intégration 