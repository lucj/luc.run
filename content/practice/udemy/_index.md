---
title: 'Udemy courses'
weight: 5
---

![Udemy](../images/udemy.png)

Les exercices présent dans cette section sont ceux utilisés dans mes cours d'introduction à [Docker](https://www.udemy.com/course/la-plateforme-docker/?referralCode=2D28CA64EF6830F08F71) et à [Kubernetes](https://www.udemy.com/course/la_plateforme_k8s/?referralCode=8DA71FBFF795B959CCA7) sur la plateforme Udemy.

<br/>

{{< cards >}}
  {{< card 
      link="./docker" 
      title="Docker" 
      subtitle="Exercices d'introduction à Docker"
      image="../images/docker.png" 
      imageStyle="margin: 1rem auto; width: 50%;" 
  >}}
  {{< card 
      link="./kubernetes" 
      title="Kubernetes" 
      subtitle="Exercices d'introduction à Kubernetes"
      image="../images/kubernetes.png" 
      imageStyle="margin: 1rem auto; width: 50%;" 
  >}}
{{< /cards >}}