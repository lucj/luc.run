---
weight: 1
---

Dans cet exercice, vous allez utiliser une ressource de type ConfigMap pour fournir un fichier de configuration à un reverse proxy basé sur nginx.

### 1. Création de l'application whoami

La spécification suivante définit un Pod, contenant un unique container basé sur l'image *lucj/whoami*, ainsi qu'un service de type *ClusterIP* dont le rôle est d'exposer ce Pod à l'intérieur du cluster.


```yaml {filename="whoami.yaml"}
apiVersion: v1
kind: Pod
metadata:
  name: poddy
  labels:
    app: whoami
spec:
  containers:
  - name: whoami
    image: lucj/whoami
---
apiVersion: v1
kind: Service
metadata:
  name: whoami
  labels:
    app: whoami
spec:
  selector:
    app: whoami
  type: ClusterIP
  ports:
  - port: 80
    targetPort: 80
```

Copiez cette spécification dans un fichier *whoami.yaml* puis créez le Pod et le Service avec la commande suivante:

```bash
kubectl apply -f whoami.yaml
```

Vérifiez ensuite que ces 2 objets ont été correctement lancés:

```bash
$ kubectl get po,svc
NAME                                 READY     STATUS    RESTARTS   AGE
pod/poddy                            1/1       Running   0          26s

NAME                    TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)          AGE
service/whoami          ClusterIP   10.11.243.238   <none>        80/TCP           18s
```

### 2. Création d'une ConfigMap

Nous allons utiliser la configuration ci-dessous pour le serveur nginx que nous mettrons en place dans la suite.

```yaml {filename="nginx.conf"}
user nginx;
worker_processes 4;
pid /run/nginx.pid;
events {
   worker_connections 768;
}
http {
  server {
    listen *:80;
    location = /whoami {
      proxy_pass http://whoami/;
    }
  }
}
```

Copiez cette configuration dans un fichier *nginx.conf*, puis lancez la commande suivante pour créer la configMap *proxy-config*:

```bash
kubectl create configmap proxy-config --from-file=./nginx.conf
```

### 3. Spécification du reverse-proxy

La spécification suivante définie un Pod, contenant un unique container basé sur l'image *nginx*, ainsi qu'un service de type *NodePort* dont le rôle est d'exposer ce Pod à l'extérieur du cluster. C'est à ce service que l'on enverra une requête HTTP dans la suite.

Comme on peut le voir, la spécification définie un volume qui est utilisé pour monter la ConfigMap *proxy-config* dans le container *proxy* et donc le configurer

```yaml {filename="proxy.yaml"}
apiVersion: v1
kind: Pod
metadata:
  name: proxy
  labels:
    app: proxy
spec:
  containers:
  - name: proxy
    image: nginx:1.20-alpine
    volumeMounts:
    - name: config
      mountPath: "/etc/nginx/"
  volumes:
  - name: config
    configMap:
      name: proxy-config
---
apiVersion: v1
kind: Service
metadata:
  name: proxy
  labels:
    app: proxy
spec:
  selector:
    app: proxy
  type: NodePort
  ports:
  - port: 80
    targetPort: 80
    nodePort: 31600
```

Copiez cette spécification dans un fichier *proxy.yaml* puis créez le Pod et le Service:

```bash
kubectl apply -f proxy.yaml
```

Vérifiez ensuite que ces 2 objets ont été correctement lancés:

```bash
$ kubectl get po,svc
NAME                                 READY     STATUS    RESTARTS   AGE
pod/proxy                            1/1       Running   0          17s
pod/poddy                            1/1       Running   0          2m

NAME                    TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)          AGE
service/proxy           NodePort    10.11.255.14    <none>        80:31600/TCP     17s
service/whoami          ClusterIP   10.11.243.238   <none>        80/TCP           18m
```

### 4. Test de l'application

Depuis l'IP d'une des machines du cluster nous pouvons alors envoyer une requête GET sur le endpoint */whoami* et voir que celle-ci est bien traitée par l'application *whoami*, elle renvoie *poddy*, le nom du Pod.

Utilisez la commande `kubectl get nodes -o wide` pour obtenir les adresses IP des machines du cluster et remplacez *HOST_IP* par l'une d'entre elles:

```bash
curl HOST_IP:31600/whoami
```

### 5. Cleanup

Supprimez les différents ressources créées:

```bash
kubectl delete -f proxy.yaml
kubectl delete -f whoami.yaml
kubectl delete cm proxy-config
```

### En résumé

Nous avons donc utilisé un objet ConfigMap que nous avons monté dans le container nginx du Pod faisant office de reverse-proxy. Cela permet de découpler la configuration et l'application.
