---
weight: 1
---

Afin de pouvoir installer des applications packagées dans le format définit par *Helm*, vous allez installer le client *helm* en local. Pour cela, téléchargez la dernière version de la version *3* du client *helm* depuis la page de releases suivante:

[https://github.com/helm/helm/releases](https://github.com/helm/helm/releases)

Copiez ensuite le binaire *helm* dans votre *PATH*

Vérifiez que le client est correctement installé:

```bash
$ helm version
version.BuildInfo{Version:"v3.16.3", GitCommit:"cfd07493f46efc9debd9cc1b02a0961186df7fdf", GitTreeState:"dirty", GoVersion:"go1.23.3"}
```