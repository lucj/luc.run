---
title: "Practice"
disable_sidebar: true
layout: "single"
---

Whether you’re just discovering Kubernetes or preparing for the CKAD (Certified Kubernetes Application Developer) or the CKA (Certified Kubernetes Administrator), you’ll find many helpful exercises here. You'll also find useful resource to get started using Docker.

<br>

{{< cards cols="3" >}}

{{< card
title="Introduction to Docker"
image="./images/docker.png" 
imageStyle="margin: 1rem auto; width: 50%;"
link="./docker/">}}

{{< card
title="Introduction to Kubernetes"
image="./images/kubernetes.png" 
imageStyle="margin: 1rem auto; width: 50%;"
link="./k8s/">}}

{{< card
title="Preparation for the CKAD"
image="./images/logo_ckad.png" 
imageStyle="margin: 1rem auto; width: 50%;"
link="./ckad/">}}

{{< card
title="Preparation for the CKA"
image="./images/logo_cka.png" 
imageStyle="margin: 1rem auto; width: 50%;"
link="./cka/">}}

{{< card
title="Exercices des cours Docker et Kubernetes sur Udemy (Français)"
image="./images/udemy.png" 
imageStyle="margin: 1rem auto; width: 60%;"
link="./udemy/">}}

{{< /cards >}}