---
title: 'Introduction to k8s'
weight: 2
---

This content allows you to learn Kubernetes and get some hands-on practice.

{{< cards >}}
    {{< card    link="./concepts" 
                title="Concepts" 
                subtitle="Learn Kubernetes' main concepts." 
                icon="book-open"
    >}}
    {{< card    link="./local" 
                title="Local cluster" 
                subtitle="Create a local Kubernetes cluster." 
                icon="kubernetes"
    >}}
   {{< card    link="./votingapp" 
                title="VotingApp" 
                subtitle="Presentation of the demo application." 
                icon="tag"
    >}}
    {{< card    link="./resources" 
                title="Main resources" 
                subtitle="Learn and practice main resources." 
                icon="code"
    >}}
    {{< card    link="./helm" 
                title="Helm" 
                subtitle="Package and distribute an application using Helm" 
                icon="cube"
    >}}
{{< /cards >}}