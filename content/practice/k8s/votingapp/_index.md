---
title: VotingApp
weight: 3
---

The VotingApp is an application mainly used for demos and follows a microservices architecture.

While this application may not adhere to all architectural best practices, it serves as a great example of an application utilizing multiple languages, various databases, and is an excellent way to learn concepts related to Docker and Kubernetes.

The VotingApp consists of 7 microservices as illustrated in the following diagram:

![Architecture](./images/architecture.png)

- vote-ui: A Vue.js frontend that allows users to choose between Cat and Dog
- vote: A backend API built with Python / Flask
- redis: A database where votes are stored
- worker: A service that retrieves votes from Redis and stores the results in a Postgres database
- db: The Postgres database where vote results are stored
- result: A backend that sends the scores to a user interface via websocket
- result-ui: An Angular frontend that displays the voting results

The container images for each microservice are available in the [DockerHub](https://hub.docker.com/u/voting). The images' tag follow a Semantic Version (vX.Y.Z).

You can view a live version of this application at [https://vote.votingapp.xyz](https://vote.votingapp.xyz).

Let's now explore [Kubernetes' main resources](../resources/)