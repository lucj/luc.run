---
title: ""
weight: 3
---

![Pod](../images/deploy.png)

{{< cards >}}
    {{< card    link="./learn" 
                title="Learn" 
                subtitle="Manage Pods lifecycle" 
    >}}
    {{< card    link="./practice" 
                title="Practice" 
                subtitle="Use Deployment in the VotingApp" 
    >}}
{{< /cards >}}