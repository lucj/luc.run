---
title: ""
weight: 1
---

![Pod](../images/pod.png)

{{< cards >}}
    {{< card    link="./learn" 
                title="Learn" 
                subtitle="Building blocks for running containers" 
    >}}
    {{< card    link="./practice" 
                title="Practice" 
                subtitle="Run the VotingApp in Pods" 
    >}}
{{< /cards >}}