---
title: ""
weight: 4
---

![Pod](../images/configuration.png)

{{< cards >}}
    {{< card    link="./learn" 
                title="Learn" 
                subtitle="Manage configuration and sensitive data" 
    >}}
    {{< card    link="./practice" 
                title="Practice" 
                subtitle="Use a Secret to secure the database connection" 
    >}}
{{< /cards >}}