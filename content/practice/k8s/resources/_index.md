---
title: Resources
weight: 4
---

In this section, you'll now explore the main Kubernetes resources. For each one, you'll have an overview of the resource and an exercise to better understand how it is used.

{{< callout type="info" >}}
Please follow this content in order as it is designed to deploy the VotingApp adding a new resource at the time.
{{< /callout >}}

<br/>

{{< resource-cards >}}
[
  {
    "title": "Pods",
    "description": "Building blocks for running containers",
    "learn": "./pod/learn/",
    "exercise": "./pod/practice/",
    "exercise_label": "Run the VotingApp in Pods",
    "image": "./images/pod.png"
  },
  {
    "title": "Services",
    "description": "Network exposure and load balancing",
    "learn": "./service/learn/",
    "exercise": "./service/practice/",
    "exercise_label": "Add Services to the VotingApp",
    "image": "./images/service.png"
  },
  {
    "title": "Deployments",
    "description": "Manage Pods lifecycle",
    "learn": "./deployment/learn/",
    "exercise": "./deployment/practice/",
    "exercise_label": "Use Deployments in the VotingApp",
    "image": "./images/deploy.png"
  },
  {
    "title": "Configuration",
    "description": "Manage configuration and sensitive data",
    "learn": "./configuration/learn/",
    "exercise": "./configuration/practice/",
    "exercise_label": "Use a Secret to secure the connection to Postgres",
    "image": "./images/configuration.png"
  },
    {
    "title": "Storage",
    "description": "Persistent storage solutions",
    "learn": "./storage/learn/",
    "exercise": "./storage/practice/",
    "exercise_label": "Persist the VotingApp's databases",
    "image": "./images/storage.png"
  },
  {
    "title": "Jobs & CronJobs",
    "description": "One-off and scheduled tasks",
    "learn": "./job/learn/",
    "exercise": "./job/practice/",
    "exercise_label": "Use a CronJob to create dummy votes",
    "image": "./images/job.png"
  },
  {
    "title": "Ingress",
    "description": "External access management",
    "learn": "./ingress/learn/",
    "exercise": "./ingress/practice/",
    "exercise_label": "Expose the VotingApp with an Ingress resource",
    "image": "./images/ingress.png"
  }
]
{{< /resource-cards >}}

In the [next section](../helm) you'll package and distribute the application using Helm.