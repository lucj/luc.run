---
title: ""
weight: 7
---

![Pod](../images/ingress.png)

{{< cards >}}
    {{< card    link="./learn" 
                title="Learn" 
                subtitle="External access management" 
    >}}
    {{< card    link="./practice" 
                title="Practice" 
                subtitle="Expose the VotingApp with an Ingress resource" 
    >}}
{{< /cards >}}