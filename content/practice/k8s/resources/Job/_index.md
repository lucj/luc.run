---
title: ""
weight: 6
---

![Pod](../images/job.png)

{{< cards >}}
    {{< card    link="./learn" 
                title="Learn" 
                subtitle="One-off and scheduled tasks" 
    >}}
    {{< card    link="./practice" 
                title="Practice" 
                subtitle="Use a CronJob to create dummy votes" 
    >}}
{{< /cards >}}