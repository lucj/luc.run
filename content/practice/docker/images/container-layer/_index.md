---
weight: 1
---

# Container's Layer

A container's layer is the read-write layer created when a container is launched. It's the layer where all changes made in the container are saved. This layer is deleted with the container and therefore should not be used as persistent storage.

## Launching a Container

Use the following command to launch an interactive shell in a container based on the *ubuntu:18.04* image.

```
docker container run -ti ubuntu:18.04
```

## Installing a Package

*figlet* is a package that takes text input and formats it in a fun way. By default, this package is not available in the ubuntu image. Verify this with the following command:

```
figlet
```

The command should give the following result:

```
bash: figlet: command not found
```

Install the *figlet* package with the following command:

```
apt-get update -y
apt-get install figlet
```

Verify that the binary works:

```
figlet Hola
```

Which should give the following result:

```
_   _       _
| | | | ___ | | __ _
| |_| |/ _ \| |/ _` |
|  _  | (_) | | (_| |
|_| |_|\___/|_|\__,_|

```

Exit the container.

```
exit
```

## Launching a New Container

Launch a new container based on *ubuntu:18.04*.

```
docker container run -ti ubuntu:18.04
```

Check if the figlet package is present.

```
figlet
```

You should get the following error:

```
bash: figlet: command not found
```

How do you explain this result?

Each container launched from the ubuntu image is different from others. The second container is different from the one in which figlet was installed. Each corresponds to an instance of the ubuntu image and has its own layer, added on top of the image layers, where all changes made in the container are saved.

Exit the container:

```
exit
```

## Restarting the Container

List the containers (running or not) on the host machine:

```
docker container ls -a
```

From this list, retrieve the ID of the container in which the figlet package was installed and restart it with the following command.

Note: the *start* command allows starting a container in the **Exited** state.

```
docker container start CONTAINER_ID
```

Launch an interactive shell in this container using the exec command.

```
docker container exec -ti CONTAINER_ID bash
```

Verify that figlet is present in this container.

```
figlet Hola
```

You can now exit the container.

```
exit
```

## Cleanup

List the containers (running or not) on the host machine:

```
docker container ls -a
```

To remove all containers, we can use the *rm* and *ls -aq* commands together. We add the -f option to force the removal of containers still running. Otherwise, we would need to stop the containers and then remove them.

```
docker container rm -f $(docker container ls -aq)
```

All containers have been removed, verify it once again with the following command:

```
docker container ls -a
```