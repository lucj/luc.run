---
weight: 2
---

## Exercise

1. Create a new directory and develop an HTTP server that exposes the */ping* endpoint on port 80 and responds with PONG.

{{< callout type="note" >}}
To save time, you can use the Node.js example from the solution! :)
{{< /callout >}}

2. In the same directory, create the Dockerfile that will be used to build the application image. This file should describe the following actions:

- specify a base image
- install the runtime corresponding to the chosen language
- install application dependencies
- copy the application code
- expose the application's listening port
- specify the command to execute to start the server

3. Build the image and tag it as *pong:1.0*

4. Launch a container based on this image by publishing port 80 to port 8080 of the host machine

5. Test the application

6. Remove the container

<br>

<details>
  <summary markdown="span">Solution</summary>

1. In this example, we chose Node.js

The following code is the content of *pong.js* (web server code)

```{filename="pong.js"}
var express = require('express');
var app = express();
app.get('/ping', function(req, res) {
    console.log("received");
    res.setHeader('Content-Type', 'text/plain');
    res.end("PONG");
});
app.listen(80);
```

The *package.json* file contains the application's dependencies (in this case, the *expressjs* library used for web application development in the NodeJs world).

```json
{
  "name": "pong",
  "version": "0.0.1",
  "main": "pong.js",
  "scripts": {
    "start": "node pong.js"
  },
   "dependencies": {  "express": "^4.14.0" }
}
```

2. A version of the Dockerfile that can be used to create an application image:

```{filename="Dockerfile"}
FROM node:16-alpine
WORKDIR /app
COPY . .
RUN npm install
EXPOSE 80
CMD ["npm", "start"]
```

Note: there are always several approaches to defining an application's *Dockerfile*. For example, we could have started with a base image like *ubuntu* or *alpine*, and installed the *nodejs* runtime as in the example below:

```{filename="Dockerfile"}
FROM alpine:3.14
RUN apk add -u npm
WORKDIR /app
COPY . .
RUN npm install
EXPOSE 80
CMD ["npm", "start"]
```

3. The following command builds the image from the previous *Dockerfile*:

```bash
docker image build -t pong:1.0 .
```

4. The following command launches a container based on the *pong:1.0* image and makes it accessible from port 8080 of the host machine:

```bash
docker container run --name pong -d -p 8080:80 pong:1.0
```

{{< callout type="warning" >}}
Make sure the port isn't already taken by another container or application. If it is, use port 8081 for example in the above command.
{{< /callout >}}

5. To test the ping server, simply send a GET request to the /ping endpoint and verify that you get PONG in return:

```bash
curl localhost:8080/ping
```

6. Remove the container using the following command:

```bash
docker rm -f pong
```

</details>