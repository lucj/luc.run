---
weight: 3
---

# Exercise

In this exercise, you will create a repository on Docker Hub and push an image inside it.

1. Create an account on Docker Hub

2. Create a repository named *www* with *public* visibility

3. Login from the command line using your Docker Hub credentials

4. Use a locally available image and tag it with USERNAME/www:1.0 (use the `docker image tag` command)

5. Upload the USERNAME/www:1.0 image to Docker Hub

6. Confirm that the image is visible from the web interface

<br>

<details>
  <summary markdown="span">Solution</summary>

1. Simply go to http://hub.docker.com and follow the account creation procedure.

2. Once logged into Docker Hub, click on the "Create Repository" button and create the repository named *www* keeping the default values.

![repo1](./images/repo1.png)

![repo2](./images/repo2.png)

3. From the command line, run the following command

```bash
docker login
```

You will then need to enter your username and password.

```bash
Login with your Docker ID to push and pull images from Docker Hub. If you don't have a Docker ID, head over to https://hub.docker.com to create one.
Username:
Password:
Login Succeeded
```

4. Select a locally available image (for example *ubuntu*, *alpine*, ... or any other image you have already downloaded or created).

Using the following command, create a new tag *USERNAME/www:1.0* that identifies this image:

Note: replace IMAGE with the local image you selected and USERNAME with your Docker Hub username.

```bash
docker image tag IMAGE USERNAME/www:1.0 
```

5. To upload the image to Docker Hub, use the push subcommand

```bash
docker image push USERNAME/www:1.0
```

The different layers of the image are uploaded to Docker Hub.

6. The new image is now visible from the Docker Hub interface

![repo3](./images/repo3.png)

As your repository is public, anyone can download (pull) the image and run it on his local machine.

</details>