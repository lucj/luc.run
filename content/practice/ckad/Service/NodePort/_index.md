---
weight: 2
---

## Exercise

In this exercise, you will create a Pod and expose it outside the cluster using a *NodePort* type Service.

1. Create a *whoami.yaml* file defining a Pod with the following properties:

- name: *whoami*
- Pod label: *app: whoami* (this label should be specified in the Pod's metadata)
- container name: *whoami*
- container image: *containous/whoami*

Then create the Pod specified in *whoami.yaml*.

2. Create a *whoami-np.yaml* file defining a service with the following characteristics:

- name: *whoami-np*
- type: *NodePort*
- a selector to group Pods with the label *app: whoami*
- forward requests to port *80* of the underlying Pods
- expose port *80* inside the cluster
- expose port *31000* on each cluster node (for external access)

Then create the Service specified in *whoami-np.yaml*

3. Launch a browser on port 31000 of one of the cluster machines.

Note: you can get the external IP addresses of your cluster nodes in the *EXTERNAL-IP* column of the output from the following command:

```
kubectl get nodes -o wide
```

![Service NodePort](../images/service_NodePort.png)

4. Delete all resources created in this exercise

<br>

<details>
  <summary markdown="span">Solution</summary>

1. The Pod specification is as follows:

```
apiVersion: v1
kind: Pod
metadata:
  name: whoami
  labels:
    app: whoami
spec:
  containers:
  - name: whoami
    image: containous/whoami
```

The following command creates the Pod:

```
kubectl apply -f whoami.yaml
```

2. The requested Service specification is as follows:

```
apiVersion: v1
kind: Service
metadata:
  name: whoami-np
  labels:
    app: whoami
spec:
  selector:
    app: whoami
  type: NodePort
  ports:
  - port: 80
    targetPort: 80
    nodePort: 31000
```

The following command launches the Service:

```
kubectl apply -f whoami-np.yaml
```

4. The resources can be deleted with the following commands:

```
kubectl delete po/whoami
kubectl delete svc/whoami-np
```
</details>