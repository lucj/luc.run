---
title: ''
weight: 2
---

![Services](./images/service.png)

This section contains various exercises on Services, the resource used to expose Pods inside or outside of the cluster.