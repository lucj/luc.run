## Exercise

In this exercise, you will create a Pod and expose it outside the cluster using a *LoadBalancer* type Service.

{{< callout type="info" >}}
this exercise can only be performed if your cluster is provisioned with a cloud provider. [This section](../../../../clusters/sks/) details the creation of a Kubernetes cluster managed by Exoscale.
{{< /callout >}}

1. Create a *ghost.yaml* file defining a Pod with the following properties:

- name: *ghost*
- Pod label: *app: ghost*
- container name: *ghost*
- container image: *ghost:4*

Then create the Pod specified in *ghost.yaml*.

2. Create a *ghost-lb.yaml* file defining a service with the following characteristics:

- name: *ghost-lb*
- type: *LoadBalancer*
- a selector to group Pods with the label *app: ghost*
- expose port *80*
- forward requests to port *2368* of the underlying Pods (ghost application's listening port)

Then create the Service specified in *ghost-lb.yaml*

3. Make sure an external IP address is associated with the *ghost-lb* Service (this may take a few tens of seconds).

4. Launch a browser using the Service's external IP address (this is the IP address associated with the LoadBalancer created to expose the service externally). This will allow you to access the Pod running the *ghost* application.

![Service LoadBalancer](../images/service_LoadBalancer.png)

5. Delete all resources created in this exercise

<br>

<details>
  <summary markdown="span">Solution</summary>

1. The Pod specification is as follows:

```
apiVersion: v1
kind: Pod
metadata:
  name: ghost
  labels:
    app: ghost
spec:
  containers:
  - name: ghost
    image: ghost:4
```

The following command creates the Pod:

```
kubectl apply -f ghost.yaml
```

2. The requested Service specification is as follows:

```
apiVersion: v1
kind: Service
metadata:
  name: ghost-lb
  labels:
    app: ghost
spec:
  selector:
    app: ghost
  type: LoadBalancer
  ports:
  - port: 80
    targetPort: 2368
```

The following command launches the Service:

```
kubectl apply -f ghost-lb.yaml
```

3. The following command provides information about the *ghost-lb* service, including its external IP address:

```
kubectl get svc ghost-lb
```

Example output where the external IP is *194.182.168.11*, this is the IP address that has been assigned to the Load balancer (infrastructure element created by the cloud provider):

```
NAME       TYPE           CLUSTER-IP     EXTERNAL-IP      PORT(S)        AGE
ghost-lb   LoadBalancer   10.99.246.92   194.182.168.11   80:32705/TCP   83s
```

This IP address can be used to access the application running in the Pod exposed by the *ghost-lb* service.

5. The resources can be deleted with the following commands:

```
kubectl delete po/ghost
kubectl delete svc/ghost-lb
```

</details>