---
weight: 1
---

## Exercise

In this exercise, you will create a Pod and expose it inside the cluster using a *ClusterIP* type Service.

1. Create a *www_pod.yaml* file defining a Pod with the following properties:

- name: *www*
- Pod label: *app: www* (this label should be specified in the Pod's metadata)
- container name: *nginx*
- container image: *nginx:1.20-alpine*

Then create the Pod specified in *www_pod.yaml*.

2. Create a *www_service_clusterIP.yaml* file defining a service with the following characteristics:

- name: *www*
- type: *ClusterIP*
- a selector to group Pods with the label *app: www*
- expose port *80* in the cluster
- forward requests to port *80* of the underlying Pods

Then create the Service specified in *www_service_clusterIP.yaml*.

3. Access the Service from within the cluster

- Launch the Pod with the following specification:

```
apiVersion: v1
kind: Pod
metadata:
  name: debug
spec:
  containers:
  - name: debug
    image: alpine:3.15
    command:
    - "sleep"
    - "10000"
```

We will use this Pod to access the *www* Service from inside the cluster. This Pod contains a single container, based on alpine and launched with the command `sleep 10000`. This container will wait for 10000 seconds. We can then launch an interactive shell inside it and test communication with the *www* Service.

- Launch the Pod using *kubectl*.

- Launch an interactive *sh* shell in the *debug* container of the Pod.

- Use *wget* to send an HTTP Get request to port *80* of the *www* service.
You should get the content, in text form, of the default *index.html* page served by *nginx*.

This shows that from within the cluster, when accessing the *www* Service, the request is properly sent to one of the Pods (we created only one here) grouped by the Service (via the *selector* key).

4. View the specification of the *www* service.

5. List the details of the *www* service

Note the existence of an entry in *Endpoints*, this corresponds to the IP of the Pod used by the Service.

Note: if multiple Pods had the label *app: www*, there would be an Endpoint entry for each of them.

6. Delete all resources created in this exercise

<br>

<details>
  <summary markdown="span">Solution</summary><br>

1. The Pod specification is as follows:

```
apiVersion: v1
kind: Pod
metadata:
  name: www
  labels:
    app: www
spec:
  containers:
  - name: nginx
    image: nginx:1.20-alpine
```

The following command creates the Pod:

```
kubectl apply -f www_pod.yaml
```

2. The requested Service specification is as follows:

```
apiVersion: v1
kind: Service
metadata:
  name: www
spec:
  selector:
    app: www
  type: ClusterIP
  ports:
  - port: 80
    targetPort: 80
```

The following command creates the Service:

```
kubectl apply -f www_service_clusterIP.yaml
```

3. Access the Service from within the cluster

- We define the following specification in the *debug_pod.yaml* file:

```
cat <<EOF > debug_pod.yaml
apiVersion: v1
kind: Pod
metadata:
  name: debug
spec:
  containers:
  - name: debug
    image: alpine:3.15
    command:
    - "sleep"
    - "10000"
EOF
```

- The following command launches the Pod:

```
kubectl apply -f debug_pod.yaml
```

- The following command launches an interactive *sh* shell in the Pod's *debug* container:

```
kubectl exec -ti debug -- sh
```

Note: If you're on Windows, this command will work correctly in Powershell. However, you'll need a utility like *winpty* to get an interactive shell in Git Bash.

- The *www* service is directly accessible inside the cluster (i.e., by Pods running on the cluster) by its name:

```
/ # wget -O- http://www
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>
```

Note: for a quicker test of Service access, we could have used the following command:
```kubectl run test --rm --restart=Never -ti --image=busybox -- wget -O - http://www```

4. The following command provides an overview of the *www* service:

```
kubectl get services www
```

Add the *-o yaml* option to get the service specification in *yaml* format:

```
kubectl get svc/www -o yaml
```

Note: the following commands are equivalent:

- kubectl get services www
- kubectl get service www
- kubectl get svc www
- kubectl get svc/www

5. The following command provides details about the *www* service:

```
kubectl describe svc/www
```

6. The resources can be deleted with the following commands:

```
kubectl delete po/www
kubectl delete svc/www
kubectl delete po/debug
```

</details>