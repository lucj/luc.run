---
weight: 10
linkTitle: RBAC
---

![RBAC](./images/logos.png)

This section includes various exercises on RBAC-related resources used to configure the authorizations in a cluster.