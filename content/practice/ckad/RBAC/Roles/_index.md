---
weight: 1
---

## Exercise

In this exercise, you will create *Role* / *RoleBinding* / *ClusterRole* / *ClusterRoleBinding* resources to grant specific permissions to a cluster user.

## Initial State

We assume a user with ID *thomas* has the necessary certificate to authenticate in the cluster. Currently, no additional rights have been given to this user, which you can verify with the following command that allows you to impersonate *thomas* and list authorized actions:

```bash
kubectl auth can-i --list --as thomas
```

You should get a result similar to this:

```bash
Resources                                       Non-Resource URLs   Resource Names   Verbs
selfsubjectaccessreviews.authorization.k8s.io   []                  []               [create]
selfsubjectrulesreviews.authorization.k8s.io    []                  []               [create]
                                                [/api/*]            []               [get]
                                                [/api]              []               [get]
                                                [/apis/*]           []               [get]
                                                [/apis]             []               [get]
                                                [/healthz]          []               [get]
                                                [/healthz]          []               [get]
                                                [/livez]            []               [get]
                                                [/livez]            []               [get]
                                                [/openapi/*]        []               [get]
                                                [/openapi]          []               [get]
                                                [/readyz]           []               [get]
                                                [/readyz]           []               [get]
                                                [/version/]         []               [get]
                                                [/version/]         []               [get]
                                                [/version]          []               [get]
                                                [/version]          []               [get]
```

This result indicates that the user is authorized to access the cluster's health status and some non-sensitive information.

## Permission to List Nodes

You will now allow *thomas* to list the cluster's nodes.

- Verify that user *thomas* doesn't have access to this action

Using the following command, confirm that *thomas* cannot perform this action:

```bash
kubectl auth can-i list nodes --as thomas
```

You should get the following message:

```bash
Warning: resource 'nodes' is not namespace scoped
no
```

- Creating the ClusterRole

Copy the following specification into the *list-nodes.yaml* file, which defines a *ClusterRole* resource allowing to list cluster nodes:

```yaml {filename="list-nodes.yaml"}
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: list-nodes
rules:
- apiGroups:
  - ""
  resources:
  - nodes
  verbs:
  - list
```

Then create this resource:

```bash
kubectl apply -f list-nodes.yaml
```

Note: you can also create this *ClusterRole* with the following imperative command:

```bash
kubectl create clusterrole list-nodes --verb list --resource nodes 
```

- Binding the ClusterRole to user *thomas*

The *ClusterRole* created previously is useless if not associated with a user. Copy the following specification into the *thomas-list-nodes.yaml* file, which defines a *ClusterRoleBinding* to associate the *ClusterRole* with the user.

```yaml {filename="thomas-list-nodes.yaml"}
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: thomas-list-nodes
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: list-nodes
subjects:
- apiGroup: rbac.authorization.k8s.io
  kind: User
  name: thomas
```

Then create this resource:

```bash
kubectl apply -f thomas-list-nodes.yaml
```

Note: you can also create this *ClusterRoleBinding* with the following imperative command:

```bash
kubectl create clusterrolebinding thomas-list-nodes --clusterrole list-nodes --user thomas
```

- Verification

As before, the following command checks if user *thomas* can list nodes:

```bash
kubectl auth can-i list nodes --as thomas
```

You should get the following message:

```bash
Warning: resource 'nodes' is not namespace scoped
yes
```

## Permission to Manage Deployments in the *dev* Namespace

You will now allow *thomas* to manipulate (create, list, update, delete) Deployments in the *dev* namespace.

- Creating the namespace

Use the following command to create the *dev* namespace:

```bash
kubectl create namespace dev
```

- Verify that user *thomas* cannot create, list, or delete Deployments in this namespace:

```bash
kubectl auth can-i create deployments.apps --as thomas --namespace dev
kubectl auth can-i get deployments.apps --as thomas --namespace dev
kubectl auth can-i list deployments.apps --as thomas --namespace dev
kubectl auth can-i update deployments.app --as thomas --namespace dev
kubectl auth can-i delete deployments.app --as thomas --namespace dev
```

Each of the above commands should return:

```bash
no
```

- Creating the Role

Copy the following specification into the *manage-deployment.yaml* file, which defines a *Role* resource allowing to manage Deployments within the *dev* namespace:

```yaml {filename="manage-deployment.yaml"}
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  name: manage-deployment
  namespace: dev
rules:
- apiGroups:
  - "apps"
  resources:
  - deployments
  verbs:
  - create
  - list
  - get
  - update
  - delete
```

Then create this resource:

```bash
kubectl apply -f manage-deployment.yaml
```

Note: you can also create this *Role* with the following imperative command:

```bash
kubectl create role manage-deployment --verb create,list,get,update,delete --resource deployments.apps --namespace dev 
```

- Binding the Role to user *thomas*

Copy the following specification into the *thomas-manage-deployment.yaml* file, which defines a *RoleBinding* to associate the previous *Role* with *thomas*:

```yaml {filename="thomas-manage-deployment.yaml"}
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: thomas-manage-deployment
  namespace: dev
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: Role
  name: manage-deployment
subjects:
- apiGroup: rbac.authorization.k8s.io
  kind: User
  name: thomas
```

Then create this resource:

```bash
kubectl apply -f thomas-manage-deployment.yaml
```

Note: you can also create this *RoleBinding* with the following imperative command:

```bash
kubectl create rolebinding thomas-manage-deployment --role manage-deployment --user thomas --namespace dev
```

- Verification

As before, the commands below verify if user *thomas* can perform various actions on Deployment resources:

```bash
kubectl auth can-i create deployments.apps --as thomas --namespace dev
kubectl auth can-i get deployments.apps --as thomas --namespace dev
kubectl auth can-i list deployments.apps --as thomas --namespace dev
kubectl auth can-i update deployments.app --as thomas --namespace dev
kubectl auth can-i delete deployments.app --as thomas --namespace dev
```

This time you should get the following message for each of these commands:

```bash
yes
```

## User's Associated Rights

As you did at the beginning of the exercise, verify the different actions that are authorized for user *thomas* in the *dev* namespace:

```bash
kubectl auth can-i --list --as thomas -n dev
```

You should get the following result:

```bash
Resources                                       Non-Resource URLs   Resource Names   Verbs
deployments.apps                                []                  []               [create list get update delete]
selfsubjectaccessreviews.authorization.k8s.io   []                  []               [create]
selfsubjectrulesreviews.authorization.k8s.io    []                  []               [create]
                                                [/api/*]            []               [get]
                                                [/api]              []               [get]
                                                [/apis/*]           []               [get]
                                                [/apis]             []               [get]
                                                [/healthz]          []               [get]
                                                [/healthz]          []               [get]
                                                [/livez]            []               [get]
                                                [/livez]            []               [get]
                                                [/openapi/*]        []               [get]
                                                [/openapi]          []               [get]
                                                [/readyz]           []               [get]
                                                [/readyz]           []               [get]
                                                [/version/]         []               [get]
                                                [/version/]         []               [get]
                                                [/version]          []               [get]
                                                [/version]          []               [get]
nodes                                           []                  []               [list]
```

In this result, you can find the Deployment management rights in the *dev* namespace as well as the right to list nodes in the cluster.