---
weight: 7
linkTitle: DaemonSet
---

## Exercise

In this exercise, you will manipulate a DaemonSet.

1. In the *log-ds.yaml* file, create a DaemonSet specification with the following characteristics:

- the DaemonSet has the name *log*
- each Pod has a single container based on the *alpine* image
- each container has access to the */var/log/* directory on the filesystem of the node on which it runs, this directory being mounted at the location */var/log/node* in the container's filesystem
- each container writes the content of *.log files (located in */var/log/node*) to its standard output

2. Create the DaemonSet defined in this specification and look at the logs of the created Pods.

Note: you can use the following documentation for help: [https://kubernetes.io/docs/concepts/workloads/controllers/daemonset/](https://kubernetes.io/docs/concepts/workloads/controllers/daemonset/).

3. Then delete the DaemonSet.

<br>

<details>
  <summary markdown="span">Solution</summary>

1. The following specification defines the DaemonSet *log* where each Pod has a single container based on the *alpine* image:

```yaml
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: log
spec:
  selector:
    matchLabels:
      app: log
  template:
    metadata:
      labels:
        app: log
    spec:
      containers:
      - name: log
        image: alpine
```

{{< callout type="warning" >}}
We don't launch this DaemonSet yet, it would result in an error as the alpine container would continuously restart.
{{< /callout >}}

We add the definition of a *hostPath* volume to access the */var/log* directory of the host machine and we make its content available in the */var/log/node* directory of the container:

```yaml
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: log
spec:
  selector:
    matchLabels:
      app: log
  template:
    metadata:
      labels:
        app: log
    spec:
      containers:
      - name: log
        image: alpine
        volumeMounts:
        - name: varlog
          mountPath: /var/log/node
      volumes:
      - name: varlog
        hostPath:
          path: /var/log
```

We add a command to make the container write the content of *.log files, present in the */var/log/nodes* directory, to its standard output:

```yaml
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: log
spec:
  selector:
    matchLabels:
      app: log
  template:
    metadata:
      labels:
        app: log
    spec:
      containers:
      - name: log
        image: alpine
        command: ["/bin/sh", "-c"]
        args: ["tail -f /var/log/node/*.log"]
        volumeMounts:
        - name: varlog
          mountPath: /var/log/node
      volumes:
      - name: varlog
        hostPath:
          path: /var/log
```

2. Once saved in the *log-ds.yaml* file, we can create this DaemonSet:

```bash
kubectl apply -f log-ds.yaml
```

We list the associated Pods:

```bash
kubectl get pods -l app=log
```

We can then see the logs of one of the Pods associated with the DaemonSet, which displays the content of the log files existing on the Node the Pod is running on. 

```bash
kubectl logs ds/log
```

In a production context, a DaemonSet could be used to deploy Pods that will read logs from each of the nodes and send them to a centralized log management solution (such as ElasticSearch, Splunk, ...)

3. We then delete the DaemonSet

```bash
kubectl delete ds log
```

</details>