---
weight: 9
---

![Ingress](./images/ingress.png)

This section contains various exercises on Ingress, the resources used to configure an Ingress Controller which is responsible for exposing application outside the cluster.