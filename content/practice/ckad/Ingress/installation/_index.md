---
weight: 1
---

## Exercise 

In this exercise, you'll install a Nginx-based Ingress Controller. An Ingress Controller is a reverse proxy that handles Ingress resources for exposing services outside the cluster. Other implementations include HAProxy and Traefik.

1. Install the appropriate Ingress Controller for your environment using the official documentation: [https://kubernetes.github.io/ingress-nginx/deploy/](https://kubernetes.github.io/ingress-nginx/deploy/)

2. Monitor the installation:

```bash
kubectl get pods -n ingress-nginx -l app.kubernetes.io/name=ingress-nginx --watch
```

{{< callout type="warning" >}}
Note: Wait until the *ingress-nginx-controller-xxx* pod shows *1/1* in *READY* column and *Running* in *STATUS* column. Use CTRL-C to exit the watch command.
{{< /callout >}}

The next exercises will involve creating Ingress resources to configure this Ingress Controller.