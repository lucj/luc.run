---
weight: 2
---

## Exercise

In this exercise, you will create an *Ingress* resource and use it to route requests to the vote and result interfaces of the VotingApp.

## Prerequisites

Ensure you have installed the nginx Ingress Controller as detailed in [the previous exercise](../installation/).

## Launching the VotingApp

Deploy version 2 of the Voting App using the following command, which references a URL pointing to a file defining all the resources:

```
kubectl apply -f https://luc.run/vote.yaml
```

## Ports of vote and result Services

The following command lists existing services:

```
$ kubectl get svc
NAME         TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)        AGE
db           ClusterIP   10.109.49.85     <none>        5432/TCP       2m31s
kubernetes   ClusterIP   10.96.0.1        <none>        443/TCP        24h
redis        ClusterIP   10.103.148.203   <none>        6379/TCP       2m31s
result       ClusterIP   10.108.23.230    <none>        5000/TCP       2m31s
result-ui    NodePort    10.101.9.144     <none>        80:31001/TCP   2m31s
vote         ClusterIP   10.98.37.242     <none>        5000/TCP       2m31s
vote-ui      NodePort    10.102.198.30    <none>        80:31000/TCP   2m31s
```

We can see that:
- The *vote-ui* Service exposes port *80* inside the cluster, and port *31000* externally.
- The *result-ui* Service exposes port *80* inside the cluster, and port *31001* externally.

## Creating the Ingress Resource

Create a file named *vote_ingress.yaml* containing the following specification:

```yaml {filename="vote-ingress.yaml"}
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: voting-domain
spec:
  ingressClassName: nginx
  rules:
  - host: vote.votingapp.com
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: vote-ui
            port:
              number: 80
  - host: result.votingapp.com
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: result-ui
            port:
              number: 80
```

The above specification defines an Ingress resource containing 2 *rules*:  

- The first specifies that requests arriving at *http://vote.votingapp.com* are forwarded to port *80* of the Service named *vote-ui*
- The second specifies that requests arriving at *http://result.votingapp.com* are forwarded to port *80* of the Service named *result-ui*

Create this resource using the following command:

```
kubectl apply -f vote_ingress.yaml
```

## Accessing the Application

In the */etc/hosts* file (if you're on Windows, it's the *C:\Windows\System32\drivers\etc\hosts* file that needs to be opened with administrator rights), define DNS resolutions for the subdomains *vote.votingapp.com* and *result.votingapp.com*. These should point to:

- The IP address of one of your cluster machines if the Ingress Controller is exposed via a NodePort type Service
- The LoadBalancer IP address if the Ingress Controller is exposed via a LoadBalancer type Service

You can now vote from the vote interface and view the results on the results interface.

- The vote interface is available:
  * at http://vote.votingapp.com (port 80) if the Ingress Controller is exposed with a LoadBalancer type service
  * at http://vote.votingapp.com:NODE-PORT, if the Ingress Controller is exposed with a *NodePort* type service (replace *NODE-PORT* with the actual service port)

- The result interface is available:
  * at http://result.votingapp.com (port 80) if the Ingress Controller is exposed with a LoadBalancer type service
  * at http://result.votingapp.com:NODE-PORT if the Ingress Controller is exposed with a *NodePort* type service (replace *NODE-PORT* with the actual service port)

## Cleanup

- Remove the Voting App using the following command:

```
kubectl delete -f https://luc.run/vote.yaml
```

- Remove the Ingress Controller by deleting the *ingress-nginx* namespace and resources that were created at the same time:

```
kubectl delete ns ingress-nginx
kubectl delete clusterrole ingress-nginx
kubectl delete clusterrolebinding ingress-nginx
kubectl delete -A ValidatingWebhookConfiguration ingress-nginx-admission
```

- Also remove the Ingress resource:

```
kubectl delete -f vote_ingress.yaml
```

### Summary

An Ingress resource allows directing HTTP traffic to different services of the application based on the domain name used. It's also possible to establish much finer rules based on the request URL. An Ingress resource can also be used to set up TLS termination.