---
title: 'Preparing the CKAD'
weight: 3
---

![CKAD](../images/logo_ckad.png)

This section contains many exercises that will help to prepare for the CKAD (Certified Kubernetes Application Developer) certification