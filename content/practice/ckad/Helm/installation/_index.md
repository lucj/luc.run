---
weight: 1
---

## Installing the Helm Client

To install applications packaged in the format defined by *Helm*, you'll need to install the *helm* client locally. To do this, download the latest version of *helm* client version *3* from the following releases page:

[https://github.com/helm/helm/releases](https://github.com/helm/helm/releases)

Then copy the *helm* binary into your *PATH*

Verify that the client is correctly installed:

```
$ helm version
version.BuildInfo{Version:"v3.3.1", GitCommit:"249e5215cde0c3fa72e27eb7a30e8d55c9696144", GitTreeState:"clean", GoVersion:"go1.14.7"}
```