---
weight: 1
---

## Exercise

The goal of this exercise is to work with namespaces and understand how to use them to isolate resources

1. Create 2 namespaces *development* and *production*

2. List the existing namespaces

3. Creating Deployments

- Create Deployment *www-0* based on nginx:1.24-alpine in the default namespace

- Create Deployment *www-1* based on nginx:1.24-alpine in the *development* namespace

- Create Deployment *www-2* based on nginx:1.24-alpine in the *production* namespace

4. List the Deployments and Pods present in the system (the `--all-namespaces` or `-A` option allows you to include all namespaces)

5. Delete the *development* and *production* namespaces as well as the *www-0* deployment created in the *default* namespace.

6. List the Deployments and Pods present in the system again. What do you notice?

Note: as we will see later, it is possible to define rules to grant access to specific actions in a namespace. This approach allows using namespaces to isolate cluster resources between multiple teams (dev, qa, prod) and/or multiple clients.

<br>

<details>
  <summary markdown="span">Solution</summary>

1. The following commands create the *development* and *production* namespaces.

```
$ kubectl create namespace development
$ kubectl create namespace production
```

2. The following command lists the namespaces present in the system.

```
kubectl get namespace
```

3. The following command creates the Deployment *www-0* in the default namespace

```
kubectl create deploy www-0 --image nginx:1.24-alpine
```

The following command creates the Deployment *www-1* in the *development* namespace

```
kubectl create deploy www-1 --image nginx:1.24-alpine --namespace development
```

The following command creates the Deployment *www-2* in the *production* namespace

```
kubectl create deploy www-2 --image nginx:1.24-alpine --namespace production
```

4. The following command lists all Deployments and Pods across all namespaces:

```
kubectl get deploy,po --all-namespaces
```

5. The following command deletes the *development* and *production* namespaces:

```
kubectl delete ns development production
```

6. If we list the Deployments and Pods, we can see that only the resources created in the *default* namespace remain. The resources from the *development* and *production* namespaces were deleted when these 2 namespaces were deleted.

The following command deletes the *www-0* deployment from the *default* namespace:

```
kubectl delete deploy www-0
```

</details>