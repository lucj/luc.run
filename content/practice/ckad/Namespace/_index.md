---
title: ''
weight: 4
---

![Namespace](./images/namespace.png)

This section contains various exercises on Namespaces, which provide a mechanism for isolating groups of resource within a same cluster.