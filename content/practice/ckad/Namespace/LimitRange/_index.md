---
weight: 3
---

## Exercise

In this exercise, we will create a namespace and define resource usage limits for each Pod that will be deployed in it.

1. Create the namespace *test*:

```
kubectl create namespace test
```

2. The following specification defines a *LimitRange* resource that sets a memory usage limit for each container that will be created in the *test* namespace. Copy this specification into the *limit.yaml* file:

```yaml {filename="limit.yaml"}
apiVersion: v1
kind: LimitRange
metadata:
  name: memory-limit-range
spec:
  limits:
  - default:
      memory: 128M
    defaultRequest:
      memory: 64M
    max:
      memory: 256M
    type: Container
```

Create the corresponding resource in the *test* namespace:

```bash
kubectl apply -f limit.yaml --namespace=test
```

3. The following specification defines a Pod with a single container that doesn't specify any resource requests or limits. Copy this specification into the www-1.yaml file:

```yaml {filename="www-1.yaml"}
apiVersion: v1
kind: Pod
metadata:
  name: www-1
spec:
  containers:
  - name: www
    image: nginx:1.18-alpine
```

Create this Pod in the *test* namespace:

```bash
kubectl apply -f www-1.yaml -n test
```

Then look at the Pod configuration:

```bash
kubectl describe po www-1 -n test
```

You should observe that *Requests* and *Limits* properties have been added to the Pod's container, with values specified in the LimitRange resource.

```yaml
...
Containers:
  www:
    Container ID:   containerd://11b259e95b68ade3e014fc994e11ea22b0a5c31d0896c845811dad4ad073ba87
    Image:          nginx:1.18-alpine
    Image ID:       docker.io/library/nginx@sha256:93baf2ec1bfefd04d29eb070900dd5d79b0f79863653453397e55a5b663a6cb1
    Port:           <none>
    Host Port:      <none>
    State:          Running
      Started:      Tue, 21 Sep 2021 19:00:11 +0200
    Ready:          True
    Restart Count:  0
    Limits:
      memory:  128M         <-- Value from .spec.limits.default.memory of LimitRange resource
    Requests:
      memory:     64M       <-- Value from .spec.limits.defaultRequest.memory of LimitRange resource
...
```

4. The following specification defines a Pod with a single container that specifies values for memory requests and limits. Copy this specification into the www-2.yaml file:

```yaml {filename="www-2.yaml"}
apiVersion: v1
kind: Pod
metadata:
  name: www-2
spec:
  containers:
  - name: www
    image: nginx:1.18-alpine
    resources:
      limits:
        memory: 120M
      requests:
        memory: 100M
```

Create this Pod in the *test* namespace:

```bash
kubectl apply -f www-2.yaml -n test
```

Then look at the Pod configuration:

```bash
kubectl describe po www-2 -n test
```

You should observe that the *Requests* and *Limits* properties have not been modified, as they are within the range accepted by the LimitRange resource.

```yaml
...
Containers:
  www:
    Container ID:   containerd://7bef304d43a3a72ffb39623b11e0f4d45f6ceeb4ee5409e047f414f22f099515
    Image:          nginx:1.18-alpine
    Image ID:       docker.io/library/nginx@sha256:93baf2ec1bfefd04d29eb070900dd5d79b0f79863653453397e55a5b663a6cb1
    Port:           <none>
    Host Port:      <none>
    State:          Running
      Started:      Tue, 21 Sep 2021 19:52:06 +0200
    Ready:          True
    Restart Count:  0
    Limits:
      memory:  120M      <-- unchanged value
    Requests:
      memory:     100M   <-- unchanged value
    Environment:  <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from kube-api-access-hmlh5 (ro)
...
```

5. The following specification defines a Pod with a single container that specifies values for memory requests and limits (with the limit being outside the range accepted by the LimitRange). Copy this specification into the www-3.yaml file:

```yaml {filename="www-3.yaml"}
apiVersion: v1
kind: Pod
metadata:
  name: www-3
spec:
  containers:
  - name: www
    image: nginx:1.18-alpine
    resources:
      limits:
        memory: 512M
      requests:
        memory: 64M
```

Then create this Pod in the *test* namespace:

```bash
kubectl apply -f www-3.yaml -n test
```

You should get a message similar to this:

```bash
Error from server (Forbidden): error when creating "www-3.yaml": pods "www-3" is forbidden: maximum memory usage per Container is 256M, but limit is 512M
```

This new Pod cannot be created because its specification contains a memory limit (.spec.containers[0].resources.limits.memory) higher than the maximum value accepted by the LimitRange.

6. Delete the *test* namespace:

```bash
kubectl delete ns test
```