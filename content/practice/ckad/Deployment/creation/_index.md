---
weight: 1
---

## Exercise

In this exercise, you will create a Deployment and expose it outside the cluster via a NodePort service.

1. Create a *ghost_deployment.yaml* file defining a Deployment with the following properties:

- name: *ghost*
- number of replicas: 3
- selector definition for the label *app: ghost*
- Pod specification:
  * label *app: ghost*
  * a container named *ghost* based on the *ghost:4* image and exposing port *2368*

Then create the specified resource.

2. Examine the status of the *ghost* Deployment.

Based on this information, what can you say about the number of Pods managed by this Deployment?

3. List the Pods associated with this Deployment.

4. Create a Service to expose the Deployment's Pods outside the cluster

Tips:

- you can start by creating a Service specification, specifying that the *selector* should group Pods with the label *app: ghost*
- use a *NodePort* type service, you can publish it on port *31001* of the cluster nodes
- the container based on the *ghost:4* image runs on port *2368*, this port should therefore be referenced as *targetPort* in the Service specification

Note: feel free to refer to the NodePort Services exercise we saw previously

Once the service is created, you will be able to access the *ghost* application interface on *http://IP:31001* where IP is the IP address of a machine in the Kubernetes cluster.

Note: you can get the IPs of your cluster machines with the command `$ kubectl get nodes -o wide`

![Ghost application interface](../images/deployment_ghost.png)

5. Delete the Deployment and Service created previously.

<br>

<details>
  <summary markdown="span">Solution</summary>

1. The specification, defined in the *ghost_deployment.yaml* file is as follows:

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: ghost
spec:
  replicas: 3
  selector:
    matchLabels:
      app: ghost
  template:
    metadata:
      labels:
        app: ghost
    spec:
      containers:
      - name: ghost
        image: ghost:4
        ports:
        - containerPort: 2368
```

The following command creates the Deployment:

```
kubectl apply -f ghost_deployment.yaml
```

2. The following command shows the Deployment status:

```
$ kubectl get deploy
NAME   READY   UP-TO-DATE   AVAILABLE   AGE
ghost   3/3     3            3          51s
```

3. The following command lists the Pods running on the cluster:

```
$ kubectl get po
NAME                    READY   STATUS    RESTARTS   AGE
ghost-548879c755-7kmpz   1/1     Running   0          68s
ghost-548879c755-m5pjt   1/1     Running   0          68s
ghost-548879c755-nwl9l   1/1     Running   0          68s
```

We can see that the 3 Pods related to the *ghost* Deployment are listed. They are all active.

4. In a *ghost_service.yaml* file we define the following specification:

```
apiVersion: v1
kind: Service
metadata:
  name: ghost
spec:
  selector:
    app: ghost
  type: NodePort
  ports:
  - port: 80
    targetPort: 2368
    nodePort: 31001
```

Then create the Service:

```
kubectl apply -f ghost_service.yaml
```

5. Delete the Deployment and Service:

```
kubectl delete deploy/ghost
kubectl delete svc/ghost
```

</details>