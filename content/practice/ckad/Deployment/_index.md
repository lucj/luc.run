---
title: ''
weight: 3
---

![Deployment](./images/deploy.png)

This section contains various exercises on Deployments, the resource used to manage the lifecycle of Pods.