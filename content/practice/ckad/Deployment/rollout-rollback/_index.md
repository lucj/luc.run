---
weight: 2
---

## Exercise

In this exercise, you will create a Deployment and perform a rolling update.

1. Using the `kubectl create` command, create a Deployment named *www* that defines a Pod based on the *nginx:1.16* image

Note: the `kubectl create` command is part of the imperative commands, it allows you to create a Deployment without using a yaml specification file. This approach is quick and should be used in development or debugging contexts. The `kubectl create` command allows you to specify the number of replicas when creating a Deployment (via the --replicas option), by default only one Pod replica will be created.

2. Change the number of replicas to have 3.

Note: for this you may need the `$ kubectl scale ...` command. The online help `$ kubectl scale --help` provides some usage examples.

3. List the resources created by the previous command (Deployment, ReplicaSet, Pod).

4. Update the nginx image to version *nginx:1.16-alpine*

Note: specify the *--record* option to keep the update history

5. List the resources again. What do you notice?

6. List the Deployment updates (= revisions).

Note: use the `kubectl rollout...` command

7. Perform a rollback and verify that the Deployment is now based on the previous image version (*nginx:1.16*)

8. Delete the *www* Deployment

<br>

<details>
  <summary markdown="span">Solution</summary>

1. The Deployment can be created with the following command:

```
kubectl create deploy www --image nginx:1.16
```

2. The following command allows you to modify the number of replicas in the Deployment:

```
kubectl scale deploy/www --replicas 3
```

3. The following command lists the Deployment, ReplicaSet and Pod.

We use the following shortcuts:

- Deployment => deploy
- ReplicaSet => rs
- Pod        => po

```
$ kubectl get deploy,rs,pod
NAME                  READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/www   3/3     3            3           20s

NAME                             DESIRED   CURRENT   READY   AGE
replicaset.apps/www-84b9d66d8d   3         3         3       20s

NAME                       READY   STATUS    RESTARTS   AGE
pod/www-84b9d66d8d-5d8wk   1/1     Running   0          20s
pod/www-84b9d66d8d-966ks   1/1     Running   0          15s
pod/www-84b9d66d8d-crf87   1/1     Running   0          15s
```

The commands from the first question created:

- 1 Deployment
- 1 ReplicaSet
- 3 Pods

The ReplicaSet ensures that the 3 Pods are active.

4. The following command updates the image to version *nginx:1.16-alpine*.

```
kubectl set image deploy/www nginx=nginx:1.16-alpine --record
```

Note: when we created the Deployment with the `kubectl create deploy` command, we didn't use a detailed specification and therefore didn't give a name to the Pod's container. However, the name *nginx* was automatically used for the container name, derived from the image used. This is the container name used in the `nginx=nginx:1.16` part of the command above.

5. We use the same command as in question 2:

```
$ kubectl get deploy,rs,pod
NAME                  READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/www   3/3     3            3           79s

NAME                             DESIRED   CURRENT   READY   AGE
replicaset.apps/www-5fd6c8cc68   3         3         3       38s
replicaset.apps/www-84b9d66d8d   0         0         0       80s

NAME                       READY   STATUS    RESTARTS   AGE
pod/www-5fd6c8cc68-7csqg   1/1     Running   0          32s
pod/www-5fd6c8cc68-d6jtc   1/1     Running   0          38s
pod/www-5fd6c8cc68-j9hmn   1/1     Running   0          35s
```

We can see here that there are now 2 ReplicaSets:

- one for managing Pods using the *nginx:1.16* image. This one is no longer active, as shown by the *0* values in the *DESIRED*, *CURRENT* and *READY* fields related to Pods managed by the ReplicaSet
- a second one that was created during the image update, it manages 3 Pods, each having a container based on the *nginx:1.16-alpine* image

6. The following command shows the command associated with the Deployment update:

```
$ kubectl rollout history deploy/www
deployment.apps/www
REVISION  CHANGE-CAUSE
1         <none>
2         kubectl set image deploy/www nginx=nginx:1.16-alpine --record=true
```

7. The following command performs a rollback and thus returns to a Pod based on the *nginx:1.16* image

```
kubectl rollout undo deploy/www
```

You can then verify the image version being used in the Deployment specification:

```
$ kubectl get deploy/www -o jsonpath='{.spec.template.spec.containers[0].image}'
nginx:1.16
```

8. The Deployment and associated resources (ReplicaSet and Pods) can be deleted with the following command:

```
kubectl delete deploy www
```

</details>