---
title: ''
weight: 1
---

![Pods](./images/pod.png)

This section contains various exercises on Pods, the smallest workload unit in Kubernetes.