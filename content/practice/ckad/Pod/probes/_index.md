---
weight: 3
---

## Exercise

1. Create a Pod specification *podinfo.yaml* with a container based on the *stefanprodan/podinfo* image. Name this Pod *podinfo*.

2. Add a *livenessProbe* that checks the port *9898* every 10 seconds after an initial delay of 30 seconds.

3. Add a *readinessProbe* that sends an HTTP GET request to the */readyz* endpoint on port *9898* every 5 seconds after an initial delay of 30 seconds.

4. Create the Pod and ensure that the container enters the *ready* state after about thirty seconds.

5. Delete the Pod.

## Documentation

[https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/](https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/)

<br>

<details>
  <summary markdown="span">Solution</summary>

1. Create a Pod specification *podinfo.yaml* with a container based on the *stefanprodan/podinfo* image. Name this Pod *podinfo*.

```
cat <<EOF > ./podinfo.yaml
apiVersion: v1
kind: Pod
metadata:
  name: podinfo
spec:
  containers:
  - image: stefanprodan/podinfo
    name: podinfo
EOF
```

Note: you can also use the following imperative command to create this specification:

```
kubectl run podinfo --image=stefanprodan/podinfo --dry-run=client -o yaml > podinfo.yaml
```

2. Add a *livenessProbe* that checks port *9898* every 10 seconds after an initial delay of 30 seconds

```
apiVersion: v1
kind: Pod
metadata:
  name: podinfo
spec:
  containers:
  - image: stefanprodan/podinfo
    name: podinfo
    livenessProbe:
      tcpSocket:
        port: 9898
      periodSeconds: 10
      initialDelaySeconds: 30
```

3. Add a *readinessProbe* that sends an HTTP GET request to the */readyz* endpoint on port *9898* every 5 seconds after an initial delay of 30 seconds

```
apiVersion: v1
kind: Pod
metadata:
  name: podinfo
spec:
  containers:
  - image: stefanprodan/podinfo
    name: podinfo
    livenessProbe:
      tcpSocket:
        port: 9898
      periodSeconds: 10
      initialDelaySeconds: 30
    readinessProbe:
      httpGet:
        path: /readyz
        port: 9898
      periodSeconds: 5
      initialDelaySeconds: 30
```

4. Create the Pod and ensure that the container enters the *ready* state after about thirty seconds.

Create the Pod:
```
kubectl apply -f podinfo.yaml
```

Verification:
```
$ kubectl get po podinfo -w
NAME      READY   STATUS    RESTARTS   AGE
...
podinfo   0/1     Running   0          20s
podinfo   1/1     Running   0          32s
```

5. Delete the Pod

```
kubectl delete po podinfo
```

</details>