---
title: 'Learn and Explore Cloud-Native Tech'
---

<div style="display: flex; justify-content: center; width: 100%; margin-bottom: 2rem;">
    <div style="text-align: center;">A collection of resources, demos, and insights focused on Kubernetes and cloud-native technologies</div>
</div>

![cover](./images/cover.png)

{{< cards cols="2" >}}
    {{< card
    title="Getting started"
    subtitle="Create local clusters for development and managed clusters for production"
    icon="kubernetes"
    link="./clusters" >}}
    {{< card
    title="Practice"
    subtitle="Exercises for getting started with Docker and Kubernetes or preparing for the CKAD and CKA certifications"
    icon="terminal"
    link="./practice" >}}
    {{< card
    title="Projects & Tools"
    subtitle="Overview of various projects & tools of the Kubernetes ecosystem"
    icon="opensource"
    link="./projects" >}}
    {{< card
    title="Sample Applications"
    subtitle="Ready-to-deploy Kubernetes applications for demonstration purposes"
    icon="webhook"
    link="./apps" >}}
    {{< card
    title="About me"
    subtitle="Professional background, sample apps, articles, and learning materials"
    icon="info"
    link="./about" >}}
{{< /cards >}}