---
title: ''
weight: 3
---

![k0s](../images/logo_k0s.png)

Using a local cluster is very handy to get started with Kubernetes, or to test things quickly. In this example, we'll use [k0s](https://k0sproject.io/), a lightweight open-source Kubernetes distribution defined as "the simple, solid & certified Kubernetes distribution that works on any infrastructure: bare-metal, on-premises, edge, IoT, public & private clouds."

We'll use [Multipass](https://multipass.run), which is a convenient tool to launch Ubuntu virtual machines on Mac, Linux, or Windows, and install k0s on this VMs.

{{< callout type="info" >}}
Please refer to [Multipass installation doc](https://canonical.com/multipass/install) to install it on your environment
{{< /callout >}}

We'll only create a single node Kubernetes, as this usually enough to get started.

## Pre-requisite

On your local machine, [install kubectl](https://kubernetes.io/docs/tasks/tools/#kubectl). It's the essential tool for communicating with a Kubernetes cluster from the command line.

## Creating an Ubuntu VM

Once you've installed Multipass, create an Ubuntu 24.04 virtual machine named *k3s* with 2G of memory allocated. This process should take a few tens of seconds.

```bash
multipass launch --name k0s --memory 2G
```

Then get the IP address of the newly created VM.

```bash
IP=$(multipass info k0s | grep IP | awk '{print $2}')
```

## Installing k0s

Run the following commands to install and run k0s in the VM.

```bash
multipass exec k0s -- bash -c "
curl -sSf https://get.k0s.sh | sudo sh
sudo k0s install controller --single
sudo k0s start"
```

{{< callout type="info" >}}
The command `curl -sfL https://get.k0s.sh | sudo sh`, used to install k0s comes from the official [k0s](https://k0sproject.io/) documentation
{{< /callout >}}

## Getting the kubeconfig file

First, generate the kubeconfig file as follows.

```bash
multipass exec k0s -- bash -c 'sudo k0s kubeconfig admin' > kubeconfig.k0s
```
Next, configure your local kubectl.

```bash
export KUBECONFIG=$PWD/kubeconfig.k0s
```

Then, list the cluster's nodes (only one in the current setup).

```bash
kubectl get no
```

Also list the Pods running in the cluster.

```bash
kubectl get pods -A
```