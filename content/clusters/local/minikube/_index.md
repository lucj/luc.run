---
title: ''
weight: 7
---

![Minikube](../images/logo_minikube.png)

Minikube is a solution that allows you to run a Kubernetes cluster locally, either in virtual machines or Docker containers.

## 1. Download the Binary

From the following link [Minikube Start](https://minikube.sigs.k8s.io/docs/start/), provide the details of your local environment and download the corresponding binary using the provided commands.

The example below shows the commands necessary to download Minikube for a MacOS / Apple Silicon environment.

![Minikube binary](../images/minikube-download.png)

## 2. Launching

:fire: There are many launch options available; you can list them with the command `minikube start --help`.

### Example 1: Single-node Cluster

The following command launches Minikube:

```
minikube start
```

Your *kubectl* client has automatically been configured to communicate with the cluster just created. You can list the single node with the following command:

```
kubectl get node
```

### Example 2: Cluster with 3 Nodes

The following command launches a 3-node cluster:

```
minikube start --nodes 3
```

As before, the nodes can be listed with the following command:

```
kubectl get no
```

## 4. Deletion

To destroy a cluster set up with Minikube, simply run the following command:

```
minikube delete
```