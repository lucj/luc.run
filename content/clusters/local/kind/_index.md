---
title: ''
weight: 5
---

![kind](../images/logo_kind.png)

Kind (Kubernetes in Docker) allows you to deploy a Kubernetes cluster where each node of the cluster runs within a Docker container.

To use it, you simply need to install *Docker* and the *kind* binary. This binary can be installed via a package manager or by downloading the pre-built binary. The different installation options are presented in the [documentation](https://kind.sigs.k8s.io/docs/user/quick-start/#installation).

## Commands

Once installed, you can view the list of available commands with the following command:

```
kind
```

You’ll get a result similar to the one below:

```
kind creates and manages local Kubernetes clusters using Docker container 'nodes'

Usage:
  kind [command]

Available Commands:
  build       Build one of [node-image]
  completion  Output shell completion code for the specified shell (bash, zsh or fish)
  create      Creates one of [cluster]
  delete      Deletes one of [cluster]
  export      Exports one of [kubeconfig, logs]
  get         Gets one of [clusters, nodes, kubeconfig]
  help        Help about any command
  load        Loads images into nodes
  version     Prints the kind CLI version

Flags:
  -h, --help              help for kind
      --loglevel string   DEPRECATED: see -v instead
  -q, --quiet             silence all stderr output
  -v, --verbosity int32   info log verbosity, higher value produces more output
      --version           version for kind

Use "kind [command] --help" for more information about a command.
```

## Creating a Single-Node Cluster

Run the following command to create a cluster (only one node in this case) within a few seconds:

```
kind create cluster --name k8s
```

Kind has automatically created a context and set it as the current context for our *kubectl* client.

```
$ kubectl config get-contexts
CURRENT   NAME         CLUSTER      AUTHINFO     NAMESPACE
*         kind-k8s     kind-k8s     kind-k8s
...
```

We can then list the nodes in the cluster (only one here):

```
kubectl get nodes
```

## HA Cluster

Kind also allows you to set up a multi-node cluster by using a configuration file. For example, the following file (*config.yaml*) defines a cluster with 3 nodes: 1 master and 2 workers.

```
# config.yaml
kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
nodes:
- role: control-plane
- role: worker
- role: worker
```

To set up this new cluster, simply specify the configuration file in the create command parameters.

```
kind create cluster --name k8s-2 --config config.yaml
```

As before, Kind has automatically created a context and set it as the current context.

```
$ kubectl config get-contexts
CURRENT   NAME         CLUSTER      AUTHINFO     NAMESPACE
          kind-k8s     kind-k8s     kind-k8s
*         kind-k8s-2   kind-k8s-2   kind-k8s-2
...
```

We can then list the nodes, which now total 3 in this new cluster:

```
kubectl get nodes
```

The following command lists the clusters that have been created:

```
kind get clusters
```

This command should return the following list:

```
k8s
k8s-2
```

## Cleanup

To delete a cluster created with *Kind*, simply run the command `kind delete cluster --name CLUSTER_NAME`.

The following commands will delete the two clusters created earlier:

```
kind delete cluster --name k8s
kind delete cluster --name k8s-2
```
