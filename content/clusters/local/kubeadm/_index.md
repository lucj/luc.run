---
title: ''
weight: 1
---

![Kubeadm](../images/logo_kubeadm.png)

{{< callout type="warning" >}}
[Multipass](https://multipass.run), a tool that allows you to spin up Ubuntu VMs in a matter of seconds on a Mac / Linux / Windows is a prerequisite to create a cluster using the script presented below.
{{< /callout >}}

This section present the script *k8s.sh*, also available on https://luc.run/k8s.sh, which allows to create a k8s cluster using kubeadm. It can be used with the default options as follows:

```
curl https://luc.run/k8s.sh | bash -s
```

It performs the following actions:

- creation of 2 Ubuntu VMs using Multipass
- installation of the required packages on each VM (kubeadm, containerd, …)
- cluster initialization on the control-plane VM
- join a worker node

It takes a couple of minutes to create the cluster depending upon the number of worker nodes (the script could probably be optimized a bit so that some actions run in parallel).

Once the creation is done you just need to configure your local kubectl with the command provided in the output:

```
export KUBECONFIG=$PWD/kubeconfig.cfg
```

Then you can use this newly created cluster and start by listing the nodes (it can take a few tens of seconds for the nodes to reach the Ready status):

```
kubectl get no
```

You can download this script and use it locally if you prefer. You can get the available options running it without parameters:

```
./k8s.sh
```

Once you’re done using the cluster you can destroy it as follows:

```
curl https://luc.run/k8s.sh | bash -s -- -D
```

or if you have retrieved the script locally

```
./k8s.sh -D
```

