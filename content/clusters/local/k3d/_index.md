---
title: ''
weight: 6
---

K3d is a tool that allows you to deploy a k3s cluster so that each node in the cluster runs in a Docker container. To use K3d, you simply need to download the associated binary and run it in an environment where Docker is installed.

### 1. Installing K3d

To install K3d, just run the following command:

```
$ curl -s https://raw.githubusercontent.com/rancher/k3d/main/install.sh | bash
```

Then check the version of k3d and the version of the k3s cluster that will be installed:

```
$ k3d version
k3d version v4.2.0
k3s version v1.20.2-k3s1 (default)
```

### 2. Creating a Cluster

The following command creates a cluster named *k3s*, containing 2 worker nodes (k3s agents) in addition to the master node (k3s server):

```
$ k3d cluster create k3s --agents 2
INFO[0000] Prep: Network
INFO[0000] Created network 'k3d-k3s'
INFO[0000] Created volume 'k3d-k3s-images'
INFO[0001] Creating node 'k3d-k3s-server-0'
INFO[0003] Pulling image 'docker.io/rancher/k3s:v1.20.2-k3s1'
INFO[0010] Creating node 'k3d-k3s-agent-0'
INFO[0010] Creating node 'k3d-k3s-agent-1'
INFO[0010] Creating LoadBalancer 'k3d-k3s-serverlb'
INFO[0012] Pulling image 'docker.io/rancher/k3d-proxy:v4.2.0'
INFO[0015] Starting cluster 'k3s'
INFO[0015] Starting servers...
INFO[0015] Starting Node 'k3d-k3s-server-0'
INFO[0022] Starting agents...
INFO[0022] Starting Node 'k3d-k3s-agent-0'
INFO[0035] Starting Node 'k3d-k3s-agent-1'
INFO[0043] Starting helpers...
INFO[0043] Starting Node 'k3d-k3s-serverlb'
INFO[0045] (Optional) Trying to get IP of the docker host and inject it into the cluster as 'host.k3d.internal' for easy access
INFO[0051] Successfully added host record to /etc/hosts in 4/4 nodes and to the CoreDNS ConfigMap
INFO[0051] Cluster 'k3s' created successfully!
INFO[0051] --kubeconfig-update-default=false --> sets --kubeconfig-switch-context=false
INFO[0051] You can now use it like this:
kubectl config use-context k3d-k3s
kubectl cluster-info
```

Many options are available when creating a cluster. These can be viewed with the following command:

```
$ k3d cluster create --help
```

Once the cluster is created, we can list the different containers that were started:

```
$ docker ps
CONTAINER ID   IMAGE                      COMMAND                  CREATED         STATUS         PORTS                             NAMES
74dbefe8f431   rancher/k3d-proxy:v4.2.0   "/bin/sh -c nginx-pr…"   3 minutes ago   Up 2 minutes   80/tcp, 0.0.0.0:56150->6443/tcp   k3d-k3s-serverlb
1042c00f61aa   rancher/k3s:v1.20.2-k3s1   "/bin/k3s agent"         3 minutes ago   Up 2 minutes                                     k3d-k3s-agent-1
dac6ab81c7f7   rancher/k3s:v1.20.2-k3s1   "/bin/k3s agent"         3 minutes ago   Up 2 minutes                                     k3d-k3s-agent-0
9fa407bacddd   rancher/k3s:v1.20.2-k3s1   "/bin/k3s server --t…"   3 minutes ago   Up 2 minutes                                     k3d-k3s-server-0
```

{{< callout type="info" >}}
A container based on the *k3d-proxy* image has been created in addition to the containers needed for k3s. This is used as a load balancer to access the cluster.
{{< /callout >}}

The following command lists the clusters created with k3d (only one in this example):

```
$ k3d cluster list
NAME   SERVERS   AGENTS   LOADBALANCER
k3s    1/1       2/2      true
```

### 3. kubeconfig

As specified in the result above, the following command configures *kubectl* to communicate with the cluster:

```
$ kubectl config use-context k3d-k3s
Switched to context "k3d-k3s"
```

We can then list the nodes in the cluster:

```
$ kubectl get nodes
NAME               STATUS   ROLES                  AGE   VERSION
k3d-k3s-agent-0    Ready    <none>                 13m   v1.20.2+k3s1
k3d-k3s-agent-1    Ready    <none>                 13m   v1.20.2+k3s1
k3d-k3s-server-0   Ready    control-plane,master   13m   v1.20.2+k3s1
```

The cluster is ready for use.

### 4. Deleting a Cluster

The following command deletes the cluster we created:

```
$ k3d cluster delete k3s
INFO[0000] Deleting cluster 'k3s'
INFO[0000] Deleted k3d-k3s-serverlb
INFO[0000] Deleted k3d-k3s-agent-1
INFO[0001] Deleted k3d-k3s-agent-0
INFO[0003] Deleted k3d-k3s-server-0
INFO[0003] Deleting cluster network 'k3d-k3s'
INFO[0003] Deleting image volume 'k3d-k3s-images'
INFO[0003] Removing cluster details from default kubeconfig...
INFO[0003] Removing standalone kubeconfig file (if there is one)...
INFO[0003] Successfully deleted cluster k3s!
```