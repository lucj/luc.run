---
title: 'Local clusters'
weight: 1
---

![Local Kubernetes](./images/solutions.png)

This section details several methods to create a local Kubernetes cluster based on different tools / distributions

{{< callout type="warning" >}}
For some of these methods, [Multipass](https://multipass.run), a tool that allows you to spin up Ubuntu VMs in a matter of seconds on a Mac / Linux / Windows is a prerequisite.
{{< /callout >}}