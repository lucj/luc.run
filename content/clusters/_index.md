---
title: 'Getting started'
disable_sidebar: true
layout: "single"
---

The first step in working with Kubernetes is creating a cluster. Whether you need a lightweight local environment for development or a production-ready platform, this page presents various options to help you get started. Local solutions are great for learning and testing, while managed services like Exoscale SKS handle the complexity of production deployments.

## Local Development Options

{{< callout >}}
**kubeadm**, **k3s**, **k0s**, and **microk8s** are deployed using [Multipass](https://multipass.run) virtual machines.
{{< /callout >}}

{{< cards cols="3" >}}

{{< card
title="Minikube"
subtitle="Easy setup to learn kubernetes"
tag="dev"
tagType="info" 
icon="cube"
link="./local/minikube/" >}}

{{< card
title="kubeadm"
subtitle="Reference tool to create a kubernetes cluster"
tag="dev"
tagType="info"
icon="cube" 
link="./local/kubeadm/" >}}

{{< card
title="k3s"
subtitle="Lightweight and IoT friendy"
tag="dev"
tagType="info"
icon="k3s" 
link="./local/k3s/" >}}

{{< card
title="k0s"
subtitle="Zero friction kubernetes distribution"
tag="dev"
tagType="info"
icon="cube" 
link="./local/k0s/" >}}

{{< card
title="MicroK8s"
subtitle="Ubuntu native kubernetes distribution"
tag="dev"
tagType="info"
icon="cube" 
link="./local/microk8s/" >}}

{{< card
title="Kind"
subtitle="Kubernetes in Docker"
tag="dev"
tagType="info"
icon="cube" 
link="./local/kind/" >}}

{{< card
title="k3d"
subtitle="k3s in Docker containers"
tag="dev"
tagType="info"
icon="cube" 
link="./local/k3d/" >}}

{{< /cards >}}

## Production Option

{{< callout type="note" >}}
For production workloads, consider using Exoscale SKS which provides a managed control plane, automated updates, load balancers, storage, DNS services and much more.
{{< /callout >}}

{{< cards >}}

{{< card
title="Exoscale SKS"
subtitle="Creating an **Exoscale SKS** cluster" 
tag="prod"
tagType="error"
icon="cube"
link="./sks/" >}}

{{< /cards >}}

<!--


{{< cards cols="2" >}}

{{< card
title="Local Development"
subtitle="Set up single-node clusters for development and testing" 
tag="dev"
tagType="info"
link="./local/">}}

{{< card
title="Production Ready"
icon="server"
subtitle="Create production-grade clusters on **Exoscale**" 
tag="prod"
tagType="error"
link="./sks/">}}

{{< /cards >}}

<br>

-->

<!---

{{< cards cols="3" >}}

{{< card
subtitle="Script to create a **kubeadm** cluster"
tag="dev"
tagType="info" 
link="./local/kubeadm/"
image="./local/images/logo_kubeadm.png">}}

{{< card
subtitle="Script to create a **k3s** cluster"
tag="dev"
tagType="info" 
link="./local/k3s/"
image="./local/images/k3s.png">}}

{{< card
subtitle="Script to create a **k0s** cluster"
tag="dev"
tagType="info" 
link="./local/k0s/"
image="./local/images/k0s.png">}}

{{< card
subtitle="Creating a **microk8s** cluster"
tag="dev"
tagType="info" 
link="./local/microk8s/"
image="./local/images/microk8s.png">}}

{{< card
subtitle="Creating a **KinD** cluster"
tag="dev"
tagType="info" 
link="./local/kind/"
image="./local/images/logo_kind.png">}}

{{< card
subtitle="Creating a **k3d** cluster"
tag="dev"
tagType="info" 
link="./local/k3d/"
image="./local/images/logo_k3d.png">}}

{{< card
subtitle="Creating a **Minikube** cluster"
tag="dev"
tagType="info" 
link="./local/minikube/"
image="./local/images/minikube.png">}}

{{< card
subtitle="Creating an **Exoscale SKS** cluster" 
tag="prod"
tagType="error"
link="./sks/"
image="./sks/images/logo_sks.png">}}

{{< /cards >}}

-->