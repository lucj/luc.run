---
title: Exoscale SKS
weight: 2
layout: "single"
---

![SKS](./images/logo_sks.png)

SKS (Scalable Kubernetes Service) is the Exoscale managed cluster offering. To create an SKS cluster, we can use one of the following methods:

- [Terraform](https://terraform.io)
- [Pulumi](https://pulumi.com)
- [exo cli](https://github.com/exoscale/cli)
- [Exoscale Portal](https://exoscale.com)

<!--
{{< cards cols="2" >}}

{{< card
title="Terraform"
link="https://terraform.io">}}

{{< card
title="Pulumi"
link="https://pulumi.com">}}

{{< card
title="exo CLI"
link="https://github.com/exoscale/cli">}}

{{< card
title="Exoscale portal"
link="https://exoscale.com">}}

{{< /cards >}}
-->

Whatever approaches we use, it creates the following resources:  

- a security group and some associated rules
- a SKS cluster
- a nodepool

We will go through the details of each approach, so you can select the one you prefer. But first you need to [create an account](./exo). 
