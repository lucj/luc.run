output "name" {
  value = var.name
}

output "zone" {
  value = var.zone
}
