variable "kube_version" {
  description = "Kubernetes version (1.21.0)"
  type        = string
  default     = "1.31.0"
}

variable "name" {
  description = "Name of the cluster"
  type        = string
  default     = "demo"
}

variable "workers_number" {
  description = "Number of workers in node pool"
  type        = number
  default     = 3
}

variable "worker_type" {
  type    = string
  default = "standard.medium"
}

variable "zone" {
  type    = string
  default = "ch-gva-2"
}
