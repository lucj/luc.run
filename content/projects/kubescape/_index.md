---
title: kubescape
weight: 9
---

In this demo, we'll explore [Kubescape](https://github.com/armosec/kubescape) to scan our cluster.

![logo](./images/kubescape.png)

## Prerequisites

We need a Kubernetes cluster, which can be created following [these instructions](../../clusters/). We also need the kubectl binary configured with the cluster's kubeconfig, and the helm binary. 

## About Kubescape

[Kubescape](https://github.com/armosec/kubescape) scans Kubernetes clusters, YAML files, and Helm charts. It detects potential issues such as:

- Configuration problems
- Vulnerabilities
- Poorly defined RBAC roles

Kubescape uses different frameworks:

- [NSA framework](https://www.nsa.gov/Press-Room/News-Highlights/Article/Article/2716980/nsa-cisa-release-kubernetes-hardening-guidance/)
- [MITRE ATT&CK®](https://www.microsoft.com/security/blog/2021/03/23/secure-containerized-environments-with-updated-threat-matrix-for-kubernetes/)
- [SOC2](https://www.armosec.io/blog/kubernetes-compliance-under-soc-2/)
- [CIS](https://www.cisecurity.org/benchmark/kubernetes)
- DevOpsBest
- ArmoBest

It can be used at various place from code to deploy.

![From code to deploy](./images/ksfromcodetodeploy.png)

## Installation

Kubescape is available for Linux / MacOS / Windows. Depending of our environment, we can use one of the [following methods](https://kubescape.io/docs/install-cli/) to install the *kubescape* binary.

- Linux

```bash
curl -s https://raw.githubusercontent.com/armosec/kubescape/master/install.sh | /bin/bash
```

- MacOS

```bash
brew tap armosec/kubescape
brew install kubescape
```

- Windows

```bash
iwr -useb https://raw.githubusercontent.com/armosec/kubescape/master/install.ps1 | iex
```

## Frameworks and controls

- List the frameworks available

```bash
$ kubescape list frameworks
┌──────────────────────┐
│ Supported frameworks │
├──────────────────────┤
│     AllControls      │
├──────────────────────┤
│       ArmoBest       │
├──────────────────────┤
│      DevOpsBest      │
├──────────────────────┤
│        MITRE         │
├──────────────────────┤
│         NSA          │
├──────────────────────┤
│         SOC2         │
├──────────────────────┤
│    cis-aks-t1.2.0    │
├──────────────────────┤
│    cis-eks-t1.2.0    │
├──────────────────────┤
│   cis-v1.23-t1.0.1   │
└──────────────────────┘
```

- List of the controls available (more than 270)

```bash
kubescape list controls
```

## Running Scans

{{< callout type="info" >}}
Kubescape communicates with the cluster associated with the current Kubernetes context
{{< /callout >}}

The following command runs a scan considering all controls available across the frameworks.

```bash
kubescape scan
```

Kubescape returns a summary of the scan and the cluster's score. The output is similar to the following.

{{% details title="Scan results" closed="true" %}}
```bash
 ✅  Initialized scanner
 ✅  Loaded policies
 ✅  Loaded exceptions
 ✅  Loaded account configurations
 ✅  Accessed Kubernetes objects
Control: C-0058 100% |████████████████████████████████████████████████████████████████████████████████████████████████████| (47/47, 76 it/s)
 ✅  Done scanning. Cluster: demo
 ✅  Done aggregating results


Security posture overview for cluster: 'demo'

In this overview, Kubescape shows you a summary of your cluster security posture, including the number of users who can perform administrative actions. For each result greater than 0, you should evaluate its need, and then define an exception to allow it. This baseline can be used to detect drift in future.

Control plane
┌────┬─────────────────────────────────────┬────────────────────────────────────┐
│    │ Control name                        │ Docs                               │
├────┼─────────────────────────────────────┼────────────────────────────────────┤
│ ✅ │ API server insecure port is enabled │ https://hub.armosec.io/docs/c-0005 │
│ ❌ │ Anonymous access enabled            │ https://hub.armosec.io/docs/c-0262 │
│ ⚠️  │ Audit logs enabled                  │ https://hub.armosec.io/docs/c-0067 │
│ ⚠️  │ RBAC enabled                        │ https://hub.armosec.io/docs/c-0088 │
│ ⚠️  │ Secret/etcd encryption enabled      │ https://hub.armosec.io/docs/c-0066 │
└────┴─────────────────────────────────────┴────────────────────────────────────┘
* failed to get cloud provider, cluster: demo

Access control
┌────────────────────────────────────────────────────┬───────────┬────────────────────────────────────┐
│ Control name                                       │ Resources │ View details                       │
├────────────────────────────────────────────────────┼───────────┼────────────────────────────────────┤
│ Administrative Roles                               │     0     │ $ kubescape scan control C-0035 -v │
│ List Kubernetes secrets                            │     2     │ $ kubescape scan control C-0015 -v │
│ Minimize access to create pods                     │     0     │ $ kubescape scan control C-0188 -v │
│ Minimize wildcard use in Roles and ClusterRoles    │     0     │ $ kubescape scan control C-0187 -v │
│ Portforwarding privileges                          │     0     │ $ kubescape scan control C-0063 -v │
│ Prevent containers from allowing command execution │     0     │ $ kubescape scan control C-0002 -v │
│ Roles with delete capabilities                     │     2     │ $ kubescape scan control C-0007 -v │
│ Validate admission controller (mutating)           │     0     │ $ kubescape scan control C-0039 -v │
│ Validate admission controller (validating)         │     0     │ $ kubescape scan control C-0036 -v │
└────────────────────────────────────────────────────┴───────────┴────────────────────────────────────┘

Secrets
┌─────────────────────────────────────────────────┬───────────┬────────────────────────────────────┐
│ Control name                                    │ Resources │ View details                       │
├─────────────────────────────────────────────────┼───────────┼────────────────────────────────────┤
│ Applications credentials in configuration files │     1     │ $ kubescape scan control C-0012 -v │
└─────────────────────────────────────────────────┴───────────┴────────────────────────────────────┘

Network
┌────────────────────────┬───────────┬────────────────────────────────────┐
│ Control name           │ Resources │ View details                       │
├────────────────────────┼───────────┼────────────────────────────────────┤
│ Missing network policy │    19     │ $ kubescape scan control C-0260 -v │
└────────────────────────┴───────────┴────────────────────────────────────┘

Workload
┌─────────────────────────┬───────────┬────────────────────────────────────┐
│ Control name            │ Resources │ View details                       │
├─────────────────────────┼───────────┼────────────────────────────────────┤
│ Host PID/IPC privileges │     1     │ $ kubescape scan control C-0038 -v │
│ HostNetwork access      │     3     │ $ kubescape scan control C-0041 -v │
│ HostPath mount          │     5     │ $ kubescape scan control C-0048 -v │
│ Non-root containers     │    18     │ $ kubescape scan control C-0013 -v │
│ Privileged container    │     4     │ $ kubescape scan control C-0057 -v │
└─────────────────────────┴───────────┴────────────────────────────────────┘


Highest-stake workloads
───────────────────────

High-stakes workloads are defined as those which Kubescape estimates would have the highest impact if they were to be exploited.

1. namespace: traefik, name: traefik, kind: Deployment
   '$ kubescape scan workload Deployment/traefik --namespace traefik'
2. namespace: kube-system, name: cilium, kind: DaemonSet
   '$ kubescape scan workload DaemonSet/cilium --namespace kube-system'
3. namespace: kube-system, name: cilium-envoy, kind: DaemonSet
   '$ kubescape scan workload DaemonSet/cilium-envoy --namespace kube-system'


Compliance Score
────────────────

The compliance score is calculated by multiplying control failures by the number of failures against supported compliance frameworks. Remediate controls, or configure your cluster baseline with exceptions, to improve this score.

* MITRE: 76.37%
* NSA: 59.67%

View a full compliance report by running '$ kubescape scan framework nsa' or '$ kubescape scan framework mitre'

```
{{% /details %}}

Several problems were identified. We can see several resources failed on controls *C-0013*. We can get additional details running the scan on this control only.

```bash
kubescape scan control C-0013 -v
```

In the current cluster (where several dummy applications are running), we get the following results showing all the Pods in error.

{{% details title="Scan results for control C-0013" closed="true" %}}
```bash

 ✅  Initialized scanner
 ✅  Loaded policies
 ✅  Loaded exceptions
 ✅  Loaded account configurations
 ✅  Accessed Kubernetes objects
 100% |████████████████████████████████████████████████████████████████████████████████████████████████████████████████████| (1/1, 781 it/s)
 ✅  Done scanning. Cluster: demo
 ✅  Done aggregating results


──────────────────────────────────────────────────


################################################################################
ApiVersion: apps/v1
Kind: DaemonSet
Name: falco
Namespace: default

Controls: 1 (Failed: 1, action required: 0)

┌───────────────────────────────────────────────────────────────────────────────────────────┐
│ Resources                                                                                 │
├───────────────────────────────────────────────────────────────────────────────────────────┤
│ Severity             : Medium                                                             │
│ Control Name         : Non-root containers                                                │
│ Docs                 : https://hub.armosec.io/docs/c-0013                                 │
│ Assisted Remediation : spec.template.spec.containers[0].securityContext.runAsNonRoot=true │
│                        spec.template.spec.containers[0].securityContext.runAsGroup=1000   │
│                        spec.template.spec.containers[1].securityContext.runAsNonRoot=true │
│                        spec.template.spec.containers[1].securityContext.runAsGroup=1000   │
└───────────────────────────────────────────────────────────────────────────────────────────┘

################################################################################
ApiVersion: apps/v1
Kind: Deployment
Name: worker
Namespace: vote

Controls: 1 (Failed: 1, action required: 0)

┌─────────────────────────────────────────────────────────────────────────────────────────┐
│ Resources                                                                               │
├─────────────────────────────────────────────────────────────────────────────────────────┤
│ Severity             : Medium                                                           │
│ Control Name         : Non-root containers                                              │
│ Docs                 : https://hub.armosec.io/docs/c-0013                               │
│ Assisted Remediation : spec.template.spec.containers[0].securityContext.runAsGroup=1000 │
└─────────────────────────────────────────────────────────────────────────────────────────┘

################################################################################
ApiVersion: apps/v1
Kind: Deployment
Name: falco-falcosidekick
Namespace: default

Controls: 1 (Failed: 1, action required: 0)

┌─────────────────────────────────────────────────────────────────────────────────────────┐
│ Resources                                                                               │
├─────────────────────────────────────────────────────────────────────────────────────────┤
│ Severity             : Medium                                                           │
│ Control Name         : Non-root containers                                              │
│ Docs                 : https://hub.armosec.io/docs/c-0013                               │
│ Assisted Remediation : spec.template.spec.containers[0].securityContext.runAsGroup=1000 │
└─────────────────────────────────────────────────────────────────────────────────────────┘

################################################################################
ApiVersion: apps/v1
Kind: DaemonSet
Name: cilium-envoy
Namespace: kube-system

Controls: 1 (Failed: 1, action required: 0)

┌───────────────────────────────────────────────────────────────────────────────────────────┐
│ Resources                                                                                 │
├───────────────────────────────────────────────────────────────────────────────────────────┤
│ Severity             : Medium                                                             │
│ Control Name         : Non-root containers                                                │
│ Docs                 : https://hub.armosec.io/docs/c-0013                                 │
│ Assisted Remediation : spec.template.spec.containers[0].securityContext.runAsNonRoot=true │
│                        spec.template.spec.containers[0].securityContext.runAsGroup=1000   │
└───────────────────────────────────────────────────────────────────────────────────────────┘

################################################################################
ApiVersion: apps/v1
Kind: Deployment
Name: db
Namespace: vote

Controls: 1 (Failed: 1, action required: 0)

┌───────────────────────────────────────────────────────────────────────────────────────────┐
│ Resources                                                                                 │
├───────────────────────────────────────────────────────────────────────────────────────────┤
│ Severity             : Medium                                                             │
│ Control Name         : Non-root containers                                                │
│ Docs                 : https://hub.armosec.io/docs/c-0013                                 │
│ Assisted Remediation : spec.template.spec.containers[0].securityContext.runAsNonRoot=true │
│                        spec.template.spec.containers[0].securityContext.runAsGroup=1000   │
└───────────────────────────────────────────────────────────────────────────────────────────┘

################################################################################
ApiVersion: apps/v1
Kind: Deployment
Name: redis
Namespace: vote

Controls: 1 (Failed: 1, action required: 0)

┌───────────────────────────────────────────────────────────────────────────────────────────┐
│ Resources                                                                                 │
├───────────────────────────────────────────────────────────────────────────────────────────┤
│ Severity             : Medium                                                             │
│ Control Name         : Non-root containers                                                │
│ Docs                 : https://hub.armosec.io/docs/c-0013                                 │
│ Assisted Remediation : spec.template.spec.containers[0].securityContext.runAsNonRoot=true │
│                        spec.template.spec.containers[0].securityContext.runAsGroup=1000   │
└───────────────────────────────────────────────────────────────────────────────────────────┘

################################################################################
ApiVersion: apps/v1
Kind: Deployment
Name: result
Namespace: vote

Controls: 1 (Failed: 1, action required: 0)

┌───────────────────────────────────────────────────────────────────────────────────────────┐
│ Resources                                                                                 │
├───────────────────────────────────────────────────────────────────────────────────────────┤
│ Severity             : Medium                                                             │
│ Control Name         : Non-root containers                                                │
│ Docs                 : https://hub.armosec.io/docs/c-0013                                 │
│ Assisted Remediation : spec.template.spec.containers[0].securityContext.runAsNonRoot=true │
│                        spec.template.spec.containers[0].securityContext.runAsGroup=1000   │
└───────────────────────────────────────────────────────────────────────────────────────────┘

################################################################################
ApiVersion: apps/v1
Kind: Deployment
Name: cilium-operator
Namespace: kube-system

Controls: 1 (Failed: 1, action required: 0)

┌───────────────────────────────────────────────────────────────────────────────────────────┐
│ Resources                                                                                 │
├───────────────────────────────────────────────────────────────────────────────────────────┤
│ Severity             : Medium                                                             │
│ Control Name         : Non-root containers                                                │
│ Docs                 : https://hub.armosec.io/docs/c-0013                                 │
│ Assisted Remediation : spec.template.spec.containers[0].securityContext.runAsNonRoot=true │
│                        spec.template.spec.containers[0].securityContext.runAsGroup=1000   │
└───────────────────────────────────────────────────────────────────────────────────────────┘

################################################################################
ApiVersion: apps/v1
Kind: DaemonSet
Name: cilium
Namespace: kube-system

Controls: 1 (Failed: 1, action required: 0)

┌───────────────────────────────────────────────────────────────────────────────────────────┐
│ Resources                                                                                 │
├───────────────────────────────────────────────────────────────────────────────────────────┤
│ Severity             : Medium                                                             │
│ Control Name         : Non-root containers                                                │
│ Docs                 : https://hub.armosec.io/docs/c-0013                                 │
│ Assisted Remediation : spec.template.spec.containers[0].securityContext.runAsNonRoot=true │
│                        spec.template.spec.containers[0].securityContext.runAsGroup=1000   │
└───────────────────────────────────────────────────────────────────────────────────────────┘

################################################################################
ApiVersion: apps/v1
Kind: Deployment
Name: vote-ui
Namespace: vote

Controls: 1 (Failed: 1, action required: 0)

┌───────────────────────────────────────────────────────────────────────────────────────────┐
│ Resources                                                                                 │
├───────────────────────────────────────────────────────────────────────────────────────────┤
│ Severity             : Medium                                                             │
│ Control Name         : Non-root containers                                                │
│ Docs                 : https://hub.armosec.io/docs/c-0013                                 │
│ Assisted Remediation : spec.template.spec.containers[0].securityContext.runAsNonRoot=true │
│                        spec.template.spec.containers[0].securityContext.runAsGroup=1000   │
└───────────────────────────────────────────────────────────────────────────────────────────┘

################################################################################
ApiVersion: apps/v1
Kind: Deployment
Name: result-ui
Namespace: vote

Controls: 1 (Failed: 1, action required: 0)

┌───────────────────────────────────────────────────────────────────────────────────────────┐
│ Resources                                                                                 │
├───────────────────────────────────────────────────────────────────────────────────────────┤
│ Severity             : Medium                                                             │
│ Control Name         : Non-root containers                                                │
│ Docs                 : https://hub.armosec.io/docs/c-0013                                 │
│ Assisted Remediation : spec.template.spec.containers[0].securityContext.runAsNonRoot=true │
│                        spec.template.spec.containers[0].securityContext.runAsGroup=1000   │
└───────────────────────────────────────────────────────────────────────────────────────────┘

################################################################################
ApiVersion: apps/v1
Kind: Deployment
Name: vote
Namespace: vote

Controls: 1 (Failed: 1, action required: 0)

┌─────────────────────────────────────────────────────────────────────────────────────────┐
│ Resources                                                                               │
├─────────────────────────────────────────────────────────────────────────────────────────┤
│ Severity             : Medium                                                           │
│ Control Name         : Non-root containers                                              │
│ Docs                 : https://hub.armosec.io/docs/c-0013                               │
│ Assisted Remediation : spec.template.spec.containers[0].securityContext.runAsGroup=1000 │
└─────────────────────────────────────────────────────────────────────────────────────────┘

################################################################################
ApiVersion: apps/v1
Kind: DaemonSet
Name: exoscale-csi-node
Namespace: kube-system

Controls: 1 (Failed: 1, action required: 0)

┌───────────────────────────────────────────────────────────────────────────────────────────┐
│ Resources                                                                                 │
├───────────────────────────────────────────────────────────────────────────────────────────┤
│ Severity             : Medium                                                             │
│ Control Name         : Non-root containers                                                │
│ Docs                 : https://hub.armosec.io/docs/c-0013                                 │
│ Assisted Remediation : spec.template.spec.containers[1].securityContext.runAsNonRoot=true │
│                        spec.template.spec.containers[1].securityContext.runAsGroup=1000   │
│                        spec.template.spec.containers[0].securityContext.runAsNonRoot=true │
│                        spec.template.spec.containers[0].securityContext.runAsGroup=1000   │
│                        spec.template.spec.containers[2].securityContext.runAsNonRoot=true │
│                        spec.template.spec.containers[2].securityContext.runAsGroup=1000   │
└───────────────────────────────────────────────────────────────────────────────────────────┘

################################################################################
ApiVersion: batch/v1
Kind: Job
Name: kube-bench
Namespace: default

Controls: 1 (Failed: 1, action required: 0)

┌───────────────────────────────────────────────────────────────────────────────────────────┐
│ Resources                                                                                 │
├───────────────────────────────────────────────────────────────────────────────────────────┤
│ Severity             : Medium                                                             │
│ Control Name         : Non-root containers                                                │
│ Docs                 : https://hub.armosec.io/docs/c-0013                                 │
│ Assisted Remediation : spec.template.spec.containers[0].securityContext.runAsNonRoot=true │
│                        spec.template.spec.containers[0].securityContext.runAsGroup=1000   │
└───────────────────────────────────────────────────────────────────────────────────────────┘

################################################################################
ApiVersion: apps/v1
Kind: StatefulSet
Name: falco-falcosidekick-ui-redis
Namespace: default

Controls: 1 (Failed: 1, action required: 0)

┌───────────────────────────────────────────────────────────────────────────────────────────┐
│ Resources                                                                                 │
├───────────────────────────────────────────────────────────────────────────────────────────┤
│ Severity             : Medium                                                             │
│ Control Name         : Non-root containers                                                │
│ Docs                 : https://hub.armosec.io/docs/c-0013                                 │
│ Assisted Remediation : spec.template.spec.containers[0].securityContext.runAsNonRoot=true │
│                        spec.template.spec.containers[0].securityContext.runAsGroup=1000   │
└───────────────────────────────────────────────────────────────────────────────────────────┘

################################################################################
ApiVersion: apps/v1
Kind: Deployment
Name: exoscale-csi-controller
Namespace: kube-system

Controls: 1 (Failed: 1, action required: 0)

┌───────────────────────────────────────────────────────────────────────────────────────────┐
│ Resources                                                                                 │
├───────────────────────────────────────────────────────────────────────────────────────────┤
│ Severity             : Medium                                                             │
│ Control Name         : Non-root containers                                                │
│ Docs                 : https://hub.armosec.io/docs/c-0013                                 │
│ Assisted Remediation : spec.template.spec.containers[2].securityContext.runAsNonRoot=true │
│                        spec.template.spec.containers[2].securityContext.runAsGroup=1000   │
│                        spec.template.spec.containers[1].securityContext.runAsNonRoot=true │
│                        spec.template.spec.containers[1].securityContext.runAsGroup=1000   │
│                        spec.template.spec.containers[5].securityContext.runAsNonRoot=true │
│                        spec.template.spec.containers[5].securityContext.runAsGroup=1000   │
│                        spec.template.spec.containers[3].securityContext.runAsNonRoot=true │
│                        spec.template.spec.containers[3].securityContext.runAsGroup=1000   │
│                        spec.template.spec.containers[0].securityContext.runAsNonRoot=true │
│                        spec.template.spec.containers[0].securityContext.runAsGroup=1000   │
│                        spec.template.spec.containers[6].securityContext.runAsNonRoot=true │
│                        spec.template.spec.containers[6].securityContext.runAsGroup=1000   │
│                        spec.template.spec.containers[4].securityContext.runAsNonRoot=true │
│                        spec.template.spec.containers[4].securityContext.runAsGroup=1000   │
└───────────────────────────────────────────────────────────────────────────────────────────┘

################################################################################
ApiVersion: v1
Kind: Pod
Name: www
Namespace: default

Controls: 1 (Failed: 1, action required: 0)

┌─────────────────────────────────────────────────────────────────────────────┐
│ Resources                                                                   │
├─────────────────────────────────────────────────────────────────────────────┤
│ Severity             : Medium                                               │
│ Control Name         : Non-root containers                                  │
│ Docs                 : https://hub.armosec.io/docs/c-0013                   │
│ Assisted Remediation : spec.containers[0].securityContext.runAsNonRoot=true │
│                        spec.containers[0].securityContext.runAsGroup=1000   │
└─────────────────────────────────────────────────────────────────────────────┘

################################################################################
ApiVersion: apps/v1
Kind: Deployment
Name: falco-falcosidekick-ui
Namespace: default

Controls: 1 (Failed: 1, action required: 0)

┌─────────────────────────────────────────────────────────────────────────────────────────┐
│ Resources                                                                               │
├─────────────────────────────────────────────────────────────────────────────────────────┤
│ Severity             : Medium                                                           │
│ Control Name         : Non-root containers                                              │
│ Docs                 : https://hub.armosec.io/docs/c-0013                               │
│ Assisted Remediation : spec.template.spec.containers[0].securityContext.runAsGroup=1000 │
└─────────────────────────────────────────────────────────────────────────────────────────┘


┌─────────────────┬───┐
│        Controls │ 1 │
│          Passed │ 0 │
│          Failed │ 1 │
│ Action Required │ 0 │
└─────────────────┴───┘

Failed resources by severity:

┌──────────┬────┐
│ Critical │ 0  │
│     High │ 0  │
│   Medium │ 18 │
│      Low │ 0  │
└──────────┴────┘

┌──────────┬─────────────────────┬──────────────────┬───────────────┬──────────────────┐
│ Severity │ Control name        │ Failed resources │ All Resources │ Compliance score │
├──────────┼─────────────────────┼──────────────────┼───────────────┼──────────────────┤
│  Medium  │ Non-root containers │        18        │      23       │       22%        │
├──────────┼─────────────────────┼──────────────────┼───────────────┼──────────────────┤
│          │  Resource Summary   │        18        │      23       │      21.74%      │
└──────────┴─────────────────────┴──────────────────┴───────────────┴──────────────────┘
```
{{% /details %}}

## Scanning options

Kubescape offers various scanning options.

- Framework selection (*mitre*, *nsa*, *cis*, *soc2*, devopsbest*, *armobest*).

```bash
kubescape scan framework nsa
```

- Specific control selection

Example for control C-0042 "SSH server running inside container"

```bash
kubescape scan control "C-0042"
```

- Namespace inclusion

```bash
kubescape scan --include-namespaces development,staging,production
```

- Namespace exclusion

```bash
kubescape scan --exclude-namespaces kube-system,kube-public
```

## Scanning Manifests

Kubescape can scan local YAML files or those accessible via URL.

Create a simple Deployment specification:

```bash
kubectl create deployment ghost --image=ghost:4 --replicas 2 --dry-run=client -o yaml > ghost.yaml
```

Scanning the file generated.

```bash
$ kubescape scan ghost.yaml

 ✅  Initialized scanner
 ✅  Loaded policies
 ✅  Loaded exceptions
 ✅  Loaded account configurations
 ✅  Done accessing local objects
Control: C-0020 100% |███████████████████████████████████████████████████████████████████████████████████████████████████| (48/48, 251 it/s)
 ✅  Done scanning File
 ✅  Done aggregating results


Security posture overview for repo: 'ghost.yaml'

Workload
┌─────────────────────┬───────────┬───────────────────────────────────────────────┐
│ Control name        │ Resources │ View details                                  │
├─────────────────────┼───────────┼───────────────────────────────────────────────┤
│ Non-root containers │     1     │ $ kubescape scan control C-0013 ghost.yaml -v │
└─────────────────────┴───────────┴───────────────────────────────────────────────┘

```

Based on the scan results, we run the specific control C-0013.

```bash
kubescape scan control C-0013 ghost.yaml -v
 ✅  Initialized scanner
 ✅  Loaded policies
 ✅  Loaded exceptions
 ✅  Loaded account configurations
 ✅  Done accessing local objects
 100% |███████████████████████████████████████████████████████████████████████████████████████████████████████████████████| (1/1, 8708 it/s)
 ✅  Done scanning File
 ✅  Done aggregating results


──────────────────────────────────────────────────


################################################################################
Source: ghost.yaml
ApiVersion: apps/v1
Kind: Deployment
Name: ghost

Controls: 1 (Failed: 1, action required: 0)

┌───────────────────────────────────────────────────────────────────────────────────────────┐
│ Resources                                                                                 │
├───────────────────────────────────────────────────────────────────────────────────────────┤
│ Severity             : Medium                                                             │
│ Control Name         : Non-root containers                                                │
│ Docs                 : https://hub.armosec.io/docs/c-0013                                 │
│ Assisted Remediation : spec.template.spec.containers[0].securityContext.runAsNonRoot=true │
│                        spec.template.spec.containers[0].securityContext.runAsGroup=1000   │
└───────────────────────────────────────────────────────────────────────────────────────────┘


┌─────────────────┬───┐
│        Controls │ 1 │
│          Passed │ 0 │
│          Failed │ 1 │
│ Action Required │ 0 │
└─────────────────┴───┘

Failed resources by severity:

┌──────────┬───┐
│ Critical │ 0 │
│     High │ 0 │
│   Medium │ 1 │
│      Low │ 0 │
└──────────┴───┘

┌──────────┬─────────────────────┬──────────────────┬───────────────┬──────────────────┐
│ Severity │ Control name        │ Failed resources │ All Resources │ Compliance score │
├──────────┼─────────────────────┼──────────────────┼───────────────┼──────────────────┤
│  Medium  │ Non-root containers │        1         │       1       │        0%        │
├──────────┼─────────────────────┼──────────────────┼───────────────┼──────────────────┤
│          │  Resource Summary   │        1         │       1       │      0.00%       │
└──────────┴─────────────────────┴──────────────────┴───────────────┴──────────────────┘
```

Following the recommendations, we modify the specification to lower the risk.

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: ghost
  name: ghost
spec:
  replicas: 2
  selector:
    matchLabels:
      app: ghost
  template:
    metadata:
      labels:
        app: ghost
    spec:
      containers:
      - image: ghost:4
        name: ghost
        securityContext:                      # Adding SecurityContext
          runAsNonRoot: true
          runAsGroup: 1000
```

We run the control once again.

```bash
$ kubescape scan control C-0013 ghost.yaml -v
 ✅  Initialized scanner
 ✅  Loaded policies
 ✅  Loaded exceptions
 ✅  Loaded account configurations
 ✅  Done accessing local objects
 100% |████████████████████████████████████████████████████████████████████████████████████████████████████████████████████| (1/1, 822 it/s)
 ✅  Done scanning File
 ✅  Done aggregating results


──────────────────────────────────────────────────



┌─────────────────┬───┐
│        Controls │ 1 │
│          Passed │ 1 │
│          Failed │ 0 │
│ Action Required │ 0 │
└─────────────────┴───┘

Failed resources by severity:

┌──────────┬───┐
│ Critical │ 0 │
│     High │ 0 │
│   Medium │ 0 │
│      Low │ 0 │
└──────────┴───┘

┌──────────┬─────────────────────┬──────────────────┬───────────────┬──────────────────┐
│ Severity │ Control name        │ Failed resources │ All Resources │ Compliance score │
├──────────┼─────────────────────┼──────────────────┼───────────────┼──────────────────┤
│  Medium  │ Non-root containers │        0         │       1       │       100%       │
├──────────┼─────────────────────┼──────────────────┼───────────────┼──────────────────┤
│          │  Resource Summary   │        0         │       1       │     100.00%      │
└──────────┴─────────────────────┴──────────────────┴───────────────┴──────────────────┘
```

## Scanning Helm Charts

To scan a Helm chart, we need to evaluate all the templates and provide the resulting YAML manifests to Kubescape.

Example commands to scan the VotingApp Helm chart:

```bash
helm template vote oci://registry-1.docker.io/voting/app --version v1.0.36 > manifest.yaml
kubescape scan manifest.yaml
```

We can scan a local Chart too.

```bash
helm create myapp 
cd myapp
kubescape scan .
```

## Scanning container images

Kubescape can scan container images for vulnerabilities. The following command scans the *nginx:1.14* image.

```bash
kubescape scan image nginx:1.24
```

{{% details title="nginx:1.24 vulnerabilities found" closed="true" %}}
```bash
✅  Successfully scanned image: nginx:1.24
┌──────────┬────────────────┬──────────────────┬─────────────────────────┬─────────────────────────┐
│ Severity │ Vulnerability  │ Component        │ Version                 │ Fixed in                │
├──────────┼────────────────┼──────────────────┼─────────────────────────┼─────────────────────────┤
│ Critical │ CVE-2023-23914 │ libcurl4         │ 7.74.0-1.3+deb11u11     │ wont-fix                │
│ Critical │ CVE-2023-23914 │ curl             │ 7.74.0-1.3+deb11u11     │ wont-fix                │
│ Critical │ CVE-2023-45853 │ zlib1g           │ 1:1.2.11.dfsg-2+deb11u2 │ wont-fix                │
│ Critical │ CVE-2024-5535  │ openssl          │ 1.1.1w-0+deb11u1        │ 1.1.1w-0+deb11u2        │
│ Critical │ CVE-2024-5535  │ libssl1.1        │ 1.1.1w-0+deb11u1        │ 1.1.1w-0+deb11u2        │
│ Critical │ CVE-2024-37371 │ libkrb5support0  │ 1.18.3-6+deb11u4        │ 1.18.3-6+deb11u5        │
│ Critical │ CVE-2024-37371 │ libkrb5-3        │ 1.18.3-6+deb11u4        │ 1.18.3-6+deb11u5        │
│ Critical │ CVE-2024-37371 │ libk5crypto3     │ 1.18.3-6+deb11u4        │ 1.18.3-6+deb11u5        │
│ Critical │ CVE-2024-37371 │ libgssapi-krb5-2 │ 1.18.3-6+deb11u4        │ 1.18.3-6+deb11u5        │
│ Critical │ CVE-2024-45491 │ libexpat1        │ 2.2.10-2+deb11u5        │ 2.2.10-2+deb11u6        │
│ Critical │ CVE-2024-45492 │ libexpat1        │ 2.2.10-2+deb11u5        │ 2.2.10-2+deb11u6        │
│ Critical │ CVE-2019-8457  │ libdb5.3         │ 5.3.28+dfsg1-0.8        │ wont-fix                │
│   High   │ CVE-2024-0553  │ libgnutls30      │ 3.7.1-5+deb11u4         │ 3.7.1-5+deb11u5         │
│   High   │ CVE-2024-37370 │ libkrb5-3        │ 1.18.3-6+deb11u4        │ 1.18.3-6+deb11u5        │
│   High   │ CVE-2024-2398  │ libcurl4         │ 7.74.0-1.3+deb11u11     │ 7.74.0-1.3+deb11u12     │
│   High   │ CVE-2022-43551 │ libcurl4         │ 7.74.0-1.3+deb11u11     │ wont-fix                │
│   High   │ CVE-2022-42916 │ libcurl4         │ 7.74.0-1.3+deb11u11     │ wont-fix                │
│   High   │ CVE-2024-2961  │ libc6            │ 2.31-13+deb11u8         │ 2.31-13+deb11u9         │
│   High   │ CVE-2024-33601 │ libc6            │ 2.31-13+deb11u8         │ 2.31-13+deb11u10        │
│   High   │ CVE-2024-33602 │ libc6            │ 2.31-13+deb11u8         │ 2.31-13+deb11u10        │
│   High   │ CVE-2024-45490 │ libexpat1        │ 2.2.10-2+deb11u5        │ 2.2.10-2+deb11u6        │
│   High   │ CVE-2023-52425 │ libexpat1        │ 2.2.10-2+deb11u5        │ 2.2.10-2+deb11u6        │
│   High   │ CVE-2022-1304  │ libext2fs2       │ 1.46.2-2                │ 1.46.2-2+deb11u1        │
│   High   │ CVE-2021-33560 │ libgcrypt20      │ 1.8.7-6                 │ wont-fix                │
│   High   │ CVE-2024-0567  │ libgnutls30      │ 3.7.1-5+deb11u4         │ 3.7.1-5+deb11u5         │
│   High   │ CVE-2022-3715  │ bash             │ 5.1-2+deb11u1           │ wont-fix                │
│   High   │ CVE-2024-2961  │ libc-bin         │ 2.31-13+deb11u8         │ 2.31-13+deb11u9         │
│   High   │ CVE-2024-37370 │ libgssapi-krb5-2 │ 1.18.3-6+deb11u4        │ 1.18.3-6+deb11u5        │
│   High   │ CVE-2024-33601 │ libc-bin         │ 2.31-13+deb11u8         │ 2.31-13+deb11u10        │
│   High   │ CVE-2024-37370 │ libk5crypto3     │ 1.18.3-6+deb11u4        │ 1.18.3-6+deb11u5        │
│   High   │ CVE-2024-33602 │ libc-bin         │ 2.31-13+deb11u8         │ 2.31-13+deb11u10        │
│   High   │ CVE-2022-1304  │ libcom-err2      │ 1.46.2-2                │ 1.46.2-2+deb11u1        │
│   High   │ CVE-2022-1304  │ e2fsprogs        │ 1.46.2-2                │ 1.46.2-2+deb11u1        │
│   High   │ CVE-2024-37370 │ libkrb5support0  │ 1.18.3-6+deb11u4        │ 1.18.3-6+deb11u5        │
│   High   │ CVE-2023-2953  │ libldap-2.4-2    │ 2.4.57+dfsg-3+deb11u1   │ wont-fix                │
│   High   │ CVE-2022-1304  │ libss2           │ 1.46.2-2                │ 1.46.2-2+deb11u1        │
│   High   │ CVE-2020-22218 │ libssh2-1        │ 1.9.0-2                 │ 1.9.0-2+deb11u1         │
│   High   │ CVE-2022-42916 │ curl             │ 7.74.0-1.3+deb11u11     │ wont-fix                │
│   High   │ CVE-2024-4741  │ libssl1.1        │ 1.1.1w-0+deb11u1        │ 1.1.1w-0+deb11u2        │
│   High   │ CVE-2024-7006  │ libtiff5         │ 4.2.0-1+deb11u5         │ wont-fix                │
│   High   │ CVE-2023-52356 │ libtiff5         │ 4.2.0-1+deb11u5         │ wont-fix                │
│   High   │ CVE-2023-52355 │ libtiff5         │ 4.2.0-1+deb11u5         │ wont-fix                │
│   High   │ CVE-2024-25062 │ libxml2          │ 2.9.10+dfsg-6.7+deb11u4 │ wont-fix                │
│   High   │ CVE-2022-2309  │ libxml2          │ 2.9.10+dfsg-6.7+deb11u4 │ 2.9.10+dfsg-6.7+deb11u5 │
│   High   │ CVE-2022-4899  │ libzstd1         │ 1.4.8+dfsg-2.1          │ wont-fix                │
│   High   │ CVE-2022-1304  │ logsave          │ 1.46.2-2                │ 1.46.2-2+deb11u1        │
│   High   │ CVE-2023-44487 │ nginx            │ 1.24.0-1~bullseye       │                         │
│   High   │ CVE-2022-43551 │ curl             │ 7.74.0-1.3+deb11u11     │ wont-fix                │
│   High   │ CVE-2024-4741  │ openssl          │ 1.1.1w-0+deb11u1        │ 1.1.1w-0+deb11u2        │
│   High   │ CVE-2023-31484 │ perl-base        │ 5.32.1-4+deb11u3        │ 5.32.1-4+deb11u4        │
│   High   │ CVE-2020-16156 │ perl-base        │ 5.32.1-4+deb11u3        │ 5.32.1-4+deb11u4        │
│   High   │ CVE-2024-2398  │ curl             │ 7.74.0-1.3+deb11u11     │ 7.74.0-1.3+deb11u12     │
└──────────┴────────────────┴──────────────────┴─────────────────────────┴─────────────────────────┘

230 vulnerabilities found
─────────────────────────

Image: nginx:1.24

* 12 Critical
* 40 High
* 56 Medium
* 122 Other


Components with most vulnerabilities
────────────────────────────────────

* libtiff5 (4.2.0-1+deb11u5) - 3 High, 8 Medium, 11 Negligible
* curl (7.74.0-1.3+deb11u11) - 1 Critical, 3 High, 5 Medium, 1 Low, 4 Negligible
* libcurl4 (7.74.0-1.3+deb11u11) - 1 Critical, 3 High, 5 Medium, 1 Low, 4 Negligible
* libc-bin (2.31-13+deb11u8) - 3 High, 2 Medium, 7 Negligible, 2 Unknown
* libc6 (2.31-13+deb11u8) - 3 High, 2 Medium, 7 Negligible, 2 Unknown
```
{{% /details %}}

## Summary

Kubescape is based on OpenPolicyAgent (https://github.com/open-policy-agent/opa). Feel free to explore its [documentation](https://kubescape.io/) to get the entire feature sets.