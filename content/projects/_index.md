---
title: ''
disable_sidebar: true
layout: "single"
---

Since Kubernetes alone doesn't provide every feature necessary for managing a fully functional and secure production environment, several additional tools are typically required to address other concerns. This section contains a collection of useful projects and tools from the Kubernetes ecosystem.

## Projects

This section provides details for selected open source projects, with a focus on CNCF ones. It doesn't explore the complete feature sets of each project, but rather illustrates their key capabilities. New projects are added regularly to give you a broader vision of the Cloud Native ecosystem.

{{< callout type="info" >}}
Some items may reference my personal repo and temp credentials as I use them for demo during meetups and other tech events.
{{< /callout >}}

<br/>

{{< cards >}}

{{< card
title="Deploy applications using Argo CD"
image="./images/argocd.svg" 
imageStyle="margin: 1rem auto; width: 50%;"
tag="gitops"
tagType="info"
link="./argo-cd/">}}

{{< card
title="Deploy applications using Flux"
image="./images/flux.png" 
imageStyle="margin: 1rem auto; width: 50%;"
tag="gitops"
tagType="info"
link="./flux/">}}

{{< card
title="Using vCluster to create Preview environments for GitLab Merge Requests"
image="./images/vcluster.png" 
imageStyle="margin: 1rem auto; width: 50%;"
link="./vcluster/">}}

{{< card
title="Automate your DNS records with ExternalDNS"
image="./images/externaldns.png" 
imageStyle="margin: 1rem auto; width: 50%;"
link="./externaldns/">}}

{{< card
title="Get a TLS Certificate with cert-manager"
image="./images/cert-manager.png" 
imageStyle="margin: 1rem auto; width: 50%;"
tag="security"
tagType="warning"
link="./cert-manager/">}}

{{< card
title="Enforcing security with Kyverno policy engine"
image="./images/kyverno.png" 
imageStyle="margin: 1rem auto; width: 50%;"
tag="security"
tagType="warning"
link="./kyverno/">}}

{{< card
title="Check if your cluster is deployed securely with kube-bench"
image="./images/kube-bench.png" 
imageStyle="margin: 1rem auto; width: 50%;"
tag="security"
tagType="warning"
link="./kube-bench/">}}

{{< card
title="Detecting security threats in real-time with Falco"
image="./images/falco.png" 
imageStyle="margin: 1rem auto; width: 50%;"
tag="security"
tagType="warning"
link="./falco/">}}

{{< card
title="Kubernetes Security from Development to Runtime with Kubescape"
image="./images/kubescape.png" 
imageStyle="margin: 1rem auto; width: 50%;"
tag="security"
tagType="warning"
link="./kubescape/">}}

{{< card
title="Static analysis of your YAML manifests with kube-score"
image="./images/kube-score.png"
imageStyle="margin: 1rem auto; width: 90%;"
tag="security"
tagType="warning"
link="./kube-score/">}}

{{< card
title="Scanning container images and Kubernetes resources with Trivy"
image="./images/trivy.png"
imageStyle="margin: 1rem auto; width: 50%;"
tag="security"
tagType="warning"
link="./trivy/">}}

{{< card
title="Sign your container image xith Cosign"
image="./images/cosign.png"
imageStyle="margin: 1rem auto; width: 50%;"
tag="security"
tagType="warning"
link="./cosign/">}}

{{< /cards >}}

## Tools

This section presents a non-exhaustive list of widely used tools organized by category like security, observability, or daily operations. 

{{< callout type="info" >}}
Some of these tools are detailed in the *Projects* section above.
{{< /callout >}}

### Daily Operations

{{< cards >}}
  {{< card 
    title="kubectx & kubens"
    subtitle="Switch between Kubernetes contexts and namespaces easily" 
    icon="terminal"
    link="https://github.com/ahmetb/kubectx" >}}
  {{< card 
    title="kubectl-aliases" 
    subtitle="Shortcuts for common kubectl commands"
    icon="terminal"
    link="https://github.com/ahmetb/kubectl-aliases" >}}
  {{< card 
    title="kube-ps1" 
    subtitle="Adds the current Kubernetes context and namespace to your shell prompt"
    icon="terminal"
    link="https://github.com/jonmosco/kube-ps1" >}}
  {{< card 
    title="Krew" 
    subtitle="Plugin manager for kubectl"
    icon="terminal"
    link="https://krew.sigs.k8s.io/" >}}
{{< /cards >}}

### Management Tools

{{< cards >}}
  {{< card 
    title="Lens" 
    subtitle="Integrated Kubernetes IDE for observability and management"
    icon="eye"
    link="https://k8slens.dev/" >}}
  {{< card 
    title="k9s" 
    subtitle="Terminal-based UI for cluster interaction"
    icon="eye"
    link="https://k9scli.io/" >}}
  {{< card 
    title="headlamp" 
    subtitle="User-friendly Kubernetes UI focused on extensibility"
    icon="eye"
    link="https://headlamp.dev/" >}}
{{< /cards >}}

### Application Management & Packaging

{{< cards >}}
  {{< card 
    title="Argo Rollout"
    subtitle="Provides advanced deployment capabilities such as blue-green, canary," 
    icon="argo"
    link="https://argo-rollouts.readthedocs.io/en/stable/" >}}

  {{< card 
    title="Reloader"
    subtitle="Watches changes in ConfigMap and Secrets and trigger rolling upgrades" 
    icon="cube"
    link="https://github.com/stakater/Reloader" >}}

  {{< card 
    title="Descheduler"
    subtitle="Based on its policy, finds pods that can be moved and evicts them" 
    icon="cube"
    link="https://github.com/kubernetes-sigs/descheduler" >}}

  {{< card 
    title="Helm"
    subtitle="Package manager for Kubernetes applications" 
    icon="helm"
    link="https://helm.sh" >}}
{{< /cards >}}

### Networking

{{< cards >}}
  {{< card 
    title="Traefik" 
    subtitle="Reverse proxy and load balancer, supporting ingress routing, SSL termination, and more"
    icon="traefik"
    link="https://doc.traefik.io/traefik/providers/kubernetes-ingress/" >}}
  {{< card 
    title="Nginx Ingress Controller" 
    subtitle="Robust ingress controller, handling HTTP(S) traffic, SSL/TLS termination, and routing"
    icon="nginx" 
    link="https://kubernetes.github.io/ingress-nginx/deploy/">}}
  {{< card 
    title="ExternalDNS" 
    subtitle="Automatically manages DNS records in your DNS provider"
    icon="cube"
    link="https://kubernetes-sigs.github.io/external-dns/v0.14.0/" >}}
{{< /cards >}}

### Security

{{< cards >}}
  {{< card 
    title="OPA Gatekeeper" 
    subtitle="Policy enforcement for Kubernetes, allowing you to define and manage policies as code"
    icon="cube"
    link="https://open-policy-agent.github.io/gatekeeper/website/docs/" >}}
  {{< card 
    title="Kyverno"
    subtitle="Kyverno policies can validate, mutate, generate, and cleanup any Kubernetes resource"  
    icon="cube"
    link="https://kyverno.io/" >}}
  {{< card 
    title="kube-bench"
    subtitle="Checks if Kubernetes is deployed securely by running tests based on the CIS Benchmark" 
    icon="cube"
    link="https://aquasecurity.github.io/kube-bench/v0.6.15/" >}}
  {{< card 
    title="Trivy" 
    subtitle="A vulnerability scanner for containers, Kubernetes, and other artifacts"
    icon="cube"
    link="https://trivy.dev/" >}}
  {{< card 
    title="kubesec" 
    subtitle="Scanning Kubernetes YAML files to find security vulnerabilities, and misconfigurations"
    icon="cube"
    link="https://kubesec.io/" >}}
  {{< card 
    title="Falco" 
    subtitle="Runtime threat detection. It monitors system calls and alerts you to abnormal behaviors"
    icon="cube"
    link="https://falco.org/" >}}
  {{< card 
    title="kubearmor"
    subtitle="Runtime Kubernetes security engine using eBPF and Linux Security Modules for securing workloads"  
    icon="cube"
    link="https://kubearmor.io/" >}}
  {{< card 
    title="kubescape" 
    subtitle="Security platform that provides security coverage across the entire application lifecycle"
    icon="cube"
    link="https://github.com/kubescape/kubescape" >}}
  {{< card 
    title="Cert-Manager" 
    subtitle="Automates the issuance and renewal of TLS certificates enabling secure exposure of applications"
    icon="cube"
    link="https://cert-manager.io/" >}}
{{< /cards >}}

### Observability

{{< cards >}}
  {{< card 
    title="Kube-Prometheus-Stack" 
    subtitle="Collection of components used to monitor a Kubernetes cluster"
    icon="prometheus"
    link="https://artifacthub.io/packages/helm/prometheus-community/kube-prometheus-stack" >}}
  {{< card 
    title="Grafana" 
    subtitle="Visualizes metrics collected by Prometheus or other data sources, with rich dashboards"
    icon="grafana"
    link="https://artifacthub.io/packages/helm/grafana/grafana" >}}
  {{< card 
    title="Jaeger" 
    subtitle="Distributed tracing for microservices"
    icon="jaeger"
    link="https://www.jaegertracing.io/" >}}
  {{< card 
    title="OpenTelemetry" 
    subtitle="High-quality, ubiquitous, and portable telemetry to enable effective observability"
    icon="opentelemetry"
    link="https://opentelemetry.io/" >}}
{{< /cards >}}

### GitOps Tools

{{< cards >}}
  {{< card 
    title="Argo CD" 
    subtitle="Declarative, GitOps continuous delivery tool for Kubernetes"
    icon="argo"
    link="https://argo-cd.readthedocs.io/en/stable/" >}}
  {{< card 
    title="Flux" 
    subtitle="Set of continuous and progressive delivery solutions for Kubernetes that are open and extensible"
    icon=""
    link="https://fluxcd.io/" >}}
{{< /cards >}}

### Cost related tools

{{< cards >}}
  {{< card 
    title="Kubecost" 
    subtitle="Provides real-time cost visibility and insights for teams using Kubernetes"
    icon="currency-dollar"
    link="https://www.kubecost.com/" >}}
  {{< card 
    title="OpenCost" 
    subtitle="Measuring and allocating cloud infrastructure and container costs in real time"
    icon="currency-dollar"
    link="https://www.opencost.io/" >}}
{{< /cards >}}

### Backup & Disaster Recovery

{{< cards >}}
  {{< card 
    title="Velero" 
    subtitle="Tool to safely back up and restore, perform disaster recovery" 
    icon="shield-check"
    link="https://velero.io/">}}
{{< /cards >}}

{{< callout type="warning" >}}
The choice of tools should align with your specific requirements and use cases. Note that some tools are complementary, while others are direct competitors and should not be used together.
{{< /callout >}}