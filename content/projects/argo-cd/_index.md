---
title: 'Argo CD'
weight: 1
linkTitle: 'Argo CD'
---

The following demonstrates the usage of Argo CD to deploy several applications the GitOps way.

![logo](./images/argocd.png)

## Prerequisites

We need a Kubernetes cluster, which can be created following [these instructions](../../clusters/). We also need the kubectl binary configured with the cluster's kubeconfig, and the helm binary. 

## Installing Argo CD

```bash
helm install argocd oci://ghcr.io/argoproj/argo-helm/argo-cd --version 7.7.0 -n argocd --create-namespace
```

## Accessing the Argo CD web UI

- get the Argo CD admin password 

```bash
kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d
```

- access the Argo CD web UI via a port-forward

```bash
kubectl port-forward service/argocd-server -n argocd 8080:443
```

## Managing sensitive data

[Doppler](https://doppler.com) is used to keep the sensitive data. [External Secret Operator](https://external-secrets.io/) is in charge of retrieving these data from Doppler and to create Kubernetes Secrets out of them.

:warn: make sure DOPPLER_SERVICE_TOKEN_PROD is set before running the following command

First we configure external-secret with the secret data source

```bash
kubectl create ns external-secrets

HISTIGNORE='*kubectl*' kubectl create secret generic doppler-token \
    --from-literal dopplerToken="$DOPPLER_SERVICE_TOKEN_PROD" \
    -n external-secrets
```

## Deploying applications

This step deploys the Argo CD Applications/ApplicationSets defined in [this GitLab repository](https://gitlab.com/lucj/gitops/-/tree/main/argocd?ref_type=heads).

```bash
kubectl apply -f https://luc.run/apps.yaml
```

{{< callout type="warning" >}}
By default, applications are deployed on specifi domain names. To provide other configuration properties, the ENV_NAME label must be set to the Argo CD's cluster and a values-ENV_NAME.yaml file created for each application.
{{< /callout >}}

The Argo CD dashboard shows all the application deployed.

![Apps in Argo CD](./images/argocd-apps.png)