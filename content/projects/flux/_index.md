---
title: 'Flux'
weight: 2
---

The following demonstrates the usage of Flux to deploy several applications the GitOps way.

![logo](./images/flux.png)

## Prerequisites

We need a Kubernetes cluster, which can be created following [these instructions](../../clusters/). We also need the kubectl binary configured with the cluster's kubeconfig, and the helm binary. 

## Installing Flux Operator

```bash
helm install flux-operator oci://ghcr.io/controlplaneio-fluxcd/charts/flux-operator --namespace flux-system --create-namespace
```

## Creating the decryption Secret

This Secret provides flux the SOPS/age key used to encrypt sensitive data. Only flux will then be able to decrypt them. This allows to have a folder containing encrypted data.

```bash
cat $SOPS_AGE_KEY_FILE |
kubectl create secret generic sops-age \
--namespace=flux-system \
--from-file=age.agekey=/dev/stdin
```

{{< callout type="note" >}}
Flux can decrypt a Secret encrypted with SOPS/age on the fly. Example to encrypt the content of a secret:
```bash
sops -e --encrypted-regex '^(data|stringData)$' --in-place secret.yaml
```
{{< /callout >}}

## Creating Secret to access the repository

As Flux needs to update the repository, we create the following Secret:

```bash
flux create secret git flux-system \
  --url=https://gitlab.com/lucj/gitops.git \
  --username=git \
  --password=$GITLAB_TOKEN
```

{{< callout type="note" >}}
A PAT with full access to GitLab API must be created and exported in the GITLAB_TOKEN environment variable first
```
export GITLAB_TOKEN=<gh-token>
```
{{< /callout >}}

## Bootstrapping the cluster

We run the following command, which create a *FluxInstance* CRD, to bootstrap the *test* environment:

```bash
kubectl apply -f https://luc.run/flux-test.yaml
```

## Monitoring the installation

```bash
kubectl get fluxreport/flux -n flux-system -o yaml
```

## Accessing the UI

After a few tens of seconds the Capacitor UI will be accessible:

```
kubectl -n flux-system port-forward svc/capacitor 9000:9000
```

![Capacitor](./images/capacitor.png)