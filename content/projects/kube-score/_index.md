---
weight: 10
---

[Kube-score](https://kube-score.com/) is a tool which performs static analysis of YAML specifications defining Kubernetes resources.

![Logo](../images/kube-score.png)

### Installation

kube-score can be installed in different ways:
- using brew ```brew install kube-score```
- downloading the latest [release](https://github.com/zegl/kube-score/releases)
- as a kubectl plugin ```kubectl krew install score```

### Available actions

Running kube-score without arguments lists the possible actions:

```bash
$ kube-score
Usage of kube-score:
kube-score [action] --flags

Actions:
	score	Checks all files in the input, and gives them a score and recommendations
	list	Prints a CSV list of all available score checks
	version	Print the version of kube-score
	help	Print this message

Run "kube-score [action] --help" for more information about a particular command
```

The following command list all the checks kube-score can perform:

```bash
kube-score list
```

These checks are also listed in the [official documentation](https://github.com/zegl/kube-score/blob/master/README_CHECKS.md)

It's possible to ignore some checks, for example:

- using *--ignore-container-cpu-limit* allows you to skip checking if containers have a CPU limit
- using *--ignore-container-memory-limit* allows you to skip checking if containers have a RAM limit

### Usage example

1. Creation of a Deployment specification, based on the *stefanprodan/podinfo* image.

```bash
kubectl create deployment podinfo --image=stefanprodan/podinfo --dry-run=client -o yaml > deploy.yaml
```

This generated the following specification:

```yaml {filename="deploy.yaml"}
apiVersion: apps/v1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    app: podinfo
  name: podinfo
spec:
  replicas: 1
  selector:
    matchLabels:
      app: podinfo
  strategy: {}
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: podinfo
    spec:
      containers:
      - image: stefanprodan/podinfo
        name: podinfo
        resources: {}
status: {}
```


2. We run kube-score on this specification

```bash
kube-score score ./deploy.yaml
```

The result is as follows:

```bash
apps/v1/Deployment podinfo                                                    💥
    path=/root/deploy.yaml
    [CRITICAL] Container Image Tag
        · podinfo -> Image with latest tag
            Using a fixed tag is recommended to avoid accidental upgrades
    [CRITICAL] Container Ephemeral Storage Request and Limit
        · podinfo -> Ephemeral Storage limit is not set
            Resource limits are recommended to avoid resource DDOS. Set resources.limits.ephemeral-storage
        · podinfo -> Ephemeral Storage request is not set
            Resource requests are recommended to make sure the application can start and run without crashing. Set resource.requests.ephemeral-storage
    [CRITICAL] Pod NetworkPolicy
        · The pod does not have a matching NetworkPolicy
            Create a NetworkPolicy that targets this pod to control who/what can communicate with this pod. Note, this feature needs to be supported by the CNI
            implementation used in the Kubernetes cluster to have an effect.
    [CRITICAL] Container Resources
        · podinfo -> CPU limit is not set
            Resource limits are recommended to avoid resource DDOS. Set resources.limits.cpu
        · podinfo -> Memory limit is not set
            Resource limits are recommended to avoid resource DDOS. Set resources.limits.memory
        · podinfo -> CPU request is not set
            Resource requests are recommended to make sure that the application can start and run without crashing. Set resources.requests.cpu
        · podinfo -> Memory request is not set
            Resource requests are recommended to make sure that the application can start and run without crashing. Set resources.requests.memory
    [CRITICAL] Container Security Context User Group ID
        · podinfo -> Container has no configured security context
            Set securityContext to run the container in a more secure context.
    [CRITICAL] Container Security Context ReadOnlyRootFilesystem
        · podinfo -> Container has no configured security context
            Set securityContext to run the container in a more secure context.
```

3. We modify the specification to remove some *CRITICAL* issues reported by kube-score

```yaml {filename="deploy.yaml"}
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: podinfo
  name: podinfo
spec:
  replicas: 1
  selector:
    matchLabels:
      app: podinfo
  strategy: {}
  template:
    metadata:
      labels:
        app: podinfo
    spec:
      containers:
      - image: stefanprodan/podinfo:6.1.0
        name: podinfo
        imagePullPolicy: Always
        securityContext:
          readOnlyRootFilesystem: true
          runAsUser: 11000
          runAsGroup: 11000
        resources:
          requests:
            cpu: 50m
            memory: 64Mi
            ephemeral-storage: 30Mi
          limits:
            cpu: 50m
            memory: 64Mi
            ephemeral-storage: 30Mi
        livenessProbe:
          httpGet:
            path: /healthz
            port: 9898
          initialDelaySeconds: 3
          periodSeconds: 3
        readinessProbe:
          httpGet:
            path: /readyz
            port: 9898
          initialDelaySeconds: 3
          periodSeconds: 3
```

We run kube-score again and verify that this new version fixes many issues.

```bash
$ kube-score score ./podinfo.yaml
apps/v1/Deployment podinfo                                                    💥
    path=/root/deploy.yaml
    [CRITICAL] Pod NetworkPolicy
        · The pod does not have a matching NetworkPolicy
            Create a NetworkPolicy that targets this pod to control who/what can communicate with this pod. Note, this feature needs to be supported by the CNI
            implementation used in the Kubernetes cluster to have an effect.
```

As indicated by this new scan, a NetworkPolicy should also be created to control incoming and outgoing communications to this Pod.

{{< callout type="warning" >}}
The previous specification has been updated to address most of the errors identified by kube-score. We need to ensure this does not disrupt the application’s functionality.
{{< /callout >}}

