---
weight: 9
---

[Trivy](https://trivy.dev) is a scanner for vulnerabilities in container images, file systems, and Git repositories, as well as for configuration issues and hard-coded secrets

You can find more information about this project in its [GitHub](https://github.com/aquasecurity/trivy) repository.

### Installation

Install *Trivy* using the following command:

```bash
curl -sfL https://raw.githubusercontent.com/aquasecurity/trivy/master/contrib/install.sh | sh -s -- -b /usr/local/bin
```

### Trivy's features

From the help command, we get the list of the available scanning commands.

```bash
$ trivy --help
...
Scanning Commands
  config      Scan config files for misconfigurations
  filesystem  Scan local filesystem
  image       Scan a container image
  kubernetes  [EXPERIMENTAL] Scan kubernetes cluster
  repository  Scan a repository
  rootfs      Scan rootfs
  sbom        Scan SBOM for vulnerabilities and licenses
  vm          [EXPERIMENTAL] Scan a virtual machine image
```

Trivy has scanners to get:
- OS packages and software dependencies 
- known CVEs (*vuln*)
- IaC issues and misconfigurations (*misconfig*)
- Sensitive information and secrets (*secret*)
- Software licenses (*license*)

{{< callout type="warning" >}}
Not all scanners are activated by default. The list of scanners to enable during a scanning operation can be provided using the *--scanners* flag.
{{< /callout >}}

Let's explore some of Trivy's features.

### Scanning an image

The following command scans the *nginx:1.24* image.

```bash
trivy image nginx:1.24
```

The screenshot below is an extract of the scanning result.

![Scanning Nginx 1.24](./images/cve-1.png)

For each vulnerability detected, a link is provided to get additional details. For example, the following information corresponds to the vulnerability *CVE-2023-23914* detected by the scan.

![CVE-2023-23914](./images/cve-2.png)

### Scanning a local helm chart

The following command scans the local folder defining the Helm chart of the webhook application

```bash
trivy config $WH/helm
```

The screenshot below is an example of the misconfiguration detected

![helm misconfiguration](./images/helm.png)

In this example, it requires some fixes to be done in the SecurityContext:
- using readOnlyRootFilesystem to prevent a process to write into the container
- using runAsUser / runAsGroup with uid/gid greater than 10000 to avoid conflict with the base system

### Scanning a Kubernetes cluster

With kubectl configured to access a cluster, we can run the following command for Trivy to scan the entire cluster and to provide a summary report.

```bash
trivy kubernetes --report summary
```

{{< callout type="warning" >}}
This process may take a couple of minutes as numerous checks are evaluated.
{{< /callout >}}

The screenshot below is an extract of the result obtained.

![Kubernetes scanning results](./images/k8s.png)

### Scanning a git repo

The following command scans the GitLab repo [https://gitlab.com/web-hook/www](https://gitlab.com/web-hook/www) which contains a Next.js application.

```bash
trivy repository https://gitlab.com/web-hook/www
```

Trivy detected some vulnerabilities in the source code.

![Repo scanning results](./images/repo.png)

### Scanning a local filesystem

Trivy can also scan a filesystem. The following command checks the source code of the webhooks/www Next.js application.

```bash
trivy fs $WH/www
```

The result is similar to the one obtained in the scan of the GitLab repo.

![Filesystem scanning results](./images/fs.png)

### Getting SBOM

The SBOM (Software Bill Of Materials) is the list of components in a piece of Software which Trivy can generate. This example gets the SBOM of an image packaging the vote-ui of the VotingApp.

```bash
trivy image --format spdx-json --output spdx.json voting/vote-ui:v1.0.35
```

This command generates the SBOM in a spdx format. The screenshot below shows an extract of this (very large) file.

![SBOM](./images/sbom.png)

### Checking Licenses

Trivy allows checking the licenses of software components. This command checks the license for the webhooks/www Next.js application.

```bash
trivy fs --scanners license --license-full $WH/www
```

The results obtained is as follows, indicating an MIT license.

```bash
2025-01-18T13:43:39+01:00       INFO    [license] Full license scanning is enabled
2025-01-18T13:43:40+01:00       INFO    [npm] To collect the license information of packages, "npm install" needs to be performed beforehand    dir="node_modules"

Loose File License(s) (license)

Total: 3 (UNKNOWN: 0, LOW: 3, MEDIUM: 0, HIGH: 0, CRITICAL: 0)

┌────────────────┬──────────┬─────────┬────────────────────────────────────────┐
│ Classification │ Severity │ License │             File Location              │
├────────────────┼──────────┼─────────┼────────────────────────────────────────┤
│ Notice         │ LOW      │ MIT     │ .next/server/pages/_document.js        │
│                │          │         ├────────────────────────────────────────┤
│                │          │         │ .next/static/chunks/pages/dashboard.js │
│                │          │         ├────────────────────────────────────────┤
│                │          │         │ LICENCE                                │
└────────────────┴──────────┴─────────┴────────────────────────────────────────
```

### Key takeaways

Trivy is a widely used tool for security. It can easily be integrated in a CI/CD pipeline to automatically triggers scans when application code is modified.
Fell free to check the [official documentation](https://trivy.dev/latest/docs/) to explore its large feature set.