---
title: "Kubectl explain"
date: 2024-11-17
tags: ["kubernetes", "tips"]
draft: false
---

Use ```kubectl explain``` to quickly explore the properties of a Kubernetes resource.

- Exploring top-level properties of a Pod

```bash
kubectl explain pod
```

- Exploring properties within the *containers* section of the specification

```bash
kubectl explain pod.spec.containers
```

This command is very handy, providing resource information directly in the terminal - no need to browse the online documentation.