---
title: "Imperative commands"
date: 2024-11-17
tags: ["kubernetes", "tips"]
draft: false
---

The recommanded way to create resources in Kubernetes is by using YAML file. YAML provides a declarative approach, making it easier to manage the configuration. However, if you're working on a local cluster and need a quicker solution, you can use kubectl imperative commands like ```kubectl create```.

## Examples of Imperative Commands

- creating a Deployment with 5 Pod replicas

```bash
kubectl create deploy my-dep --image=nginx:1.24 --replicas=5
```

- creating a Job

```bash
kubectl create job my-job --image=busybox -- echo "Hello, Kubernetes!"
```

- creating a Pod and exposing it with a ClusterIP Service at the same time

```bash
kubectl run ghost --image=ghost:4 --port=2368 --expose
```

Feel free to explore and use these commands, you can get the list with ```kubectl create --help```, to get a clearer view of all the available options.

Also, from these imperative commands you can easily retrieve the YAML specification of a resource using the options ```--dry-run=client -o yaml```. The example below allows creates a Pod specification in *pod.yaml*

```bash
kubectl run nginx --image=nginx:1.20 --dry-run=client -o yaml > pod.yaml
```

{{< callout type="info" >}}
If you're preparing for a Kubernetes certification, knowing these commands will definitely help you to save time and to get a higher score :)
{{< /callout >}}