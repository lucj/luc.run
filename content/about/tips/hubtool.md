---
title: "Hubtool"
date: 2024-11-07
tags: ["tools","docker"]
draft: false
---

[Docker Hub Tool](https://github.com/docker/hub-tool) is an experimental CLI tool for interacting with the Docker Hub. FIt's very handy to manipulate images in a specified repository.

I now use it in the CI of the [shape-it](https://gitlab.com/shape-it/www) demo application to remove images related to a specific Merge Request once this MR is merged:

```yaml
delete MR images:
  allow_failure: true
  needs: ["delete vcluster"]
  stage: cleanup
  image: lucj/hubtool:0.4.6
  before_script:
  - USER_PASS="$(printf '%s:%s' "$SHAPES_DOCKERHUB_USERNAME" "$SHAPES_DOCKERHUB_TOKEN" | base64 -w0)"
  - mkdir -p ~/.docker/
  - echo "{\"auths\":{\"hub-tool\":{\"auth\":\"$USER_PASS\"}}}" > ~/.docker/config.json
  script:
  - MR_IID=$(cat mr_iid.txt)
  - echo "Merge Request IID is ${MR_IID}"
  - echo "Listing the tags to delete"
  - hub-tool tag ls --format json lucj/shapes | jq -r ".[].Name" | awk -F':' '{print $2}' | grep -E "mr${MR_IID}-[a-z0-9]{8}$"
  - echo "Deleting the tags"
  - hub-tool tag ls --format json lucj/shapes | jq -r ".[].Name" | awk -F':' '{print $2}' | grep -E "mr${MR_IID}-[a-z0-9]{8}$" | xargs -IFOO hub-tool tag rm -f lucj/shapes:FOO
  rules:
  - if: '$CI_COMMIT_MESSAGE =~ /See merge request/ && $CI_COMMIT_BRANCH == "main"'
```