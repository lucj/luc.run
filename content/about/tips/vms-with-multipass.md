---
title: "VMs in seconds with Multipass"
date: 2024-11-17
tags: ["tools", "tips"]
draft: false
---

Wondering how to create a virtual machine on your local workstation ? Let's explore Multipass, a tool from Canonical, which makes it easy to create a VM on macOS, Linux and Windows.

## Quick introduction

Once installed, running Multipass without any arguments provides the available commands and options.

```
$ multipass
Create, control and connect to Ubuntu instances.

This is a command line utility for multipass, a
service that manages Ubuntu instances.

Options:
  -h, --help     Displays help on commandline options
  -v, --verbose  Increase logging verbosity. Repeat the 'v' in the short option
                 for more detail. Maximum verbosity is obtained with 4 (or more)
                 v's, i.e. -vvvv.

Available commands:
  alias         Create an alias
  aliases       List available aliases
  authenticate  Authenticate client
  delete        Delete instances and snapshots
  exec          Run a command on an instance
  find          Display available images to create instances from
  get           Get a configuration setting
  help          Display help about a command
  info          Display information about instances or snapshots
  launch        Create and start an Ubuntu instance
  list          List all available instances or snapshots
  mount         Mount a local directory in the instance
  networks      List available network interfaces
  prefer        Switch the current alias context
  purge         Purge all deleted instances permanently
  recover       Recover deleted instances
  restart       Restart instances
  restore       Restore an instance from a snapshot
  set           Set a configuration setting
  shell         Open a shell on a running instance
  snapshot      Take a snapshot of an instance
  start         Start instances
  stop          Stop running instances
  suspend       Suspend running instances
  transfer      Transfer files between the host and instances
  umount        Unmount a directory from an instance
  unalias       Remove aliases
  version       Show version details
```

Among the most useful commands:
- Managing the VM lifecycle (create, stop, start, restart, delete)
- Running a shell in the VM
- Get information of the specific VM
- Mounting in the VM file system a folder existing on host
- Copying files between the VM and the host

## Main commands

- creating a VM (named *demo*)

```
multipass launch -n demo
```

- running a shell in this VM

```
multipass shell demo
```

- getting information about this VM

```
multipass info 
```

## Advanced features

Multipass also allows to:
- use specific flags to allocate resources to the VM (RAM, CPU)
- use cloud-init file to configure the VM at creation

Fell free to test Multipass and play with the commands available.