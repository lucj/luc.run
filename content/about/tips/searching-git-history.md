---
title: "Searching git history"
date: 2024-11-17
tags: ["git", "tips"]
draft: false
---

Sometimes we need to search for specific content within a project's git history. Here are a few useful commands to help searching through commits and code.

## Searching in code

The generic command allows searching for a specific string across all commits in a repository:

```bash
git grep <pattern> $(git rev-list --all)
```

Let's say we know the history contains the string **kaniko** (as we used [Kaniko](https://github.com/GoogleContainerTools/kaniko) to build our container images at some point in time):

```bash
git grep kaniko $(git rev-list --all)
```

This returns a list of commit ID followed by the name of the file containing **kaniko**:

```bash
c45527b381def7ebfffb92bbd2c1ffff8d0d1ad4:.gitlab-ci.yml:    - buildah login --tls-verify=false -u "gitlab-ci-token" -p "$CI_JOB_TOKEN" $CI_REGISTRY
c45527b381def7ebfffb92bbd2c1ffff8d0d1ad4:.gitlab-ci.yml:    - buildah bud --tls-verify=false -f Dockerfile.dotnet -t $CI_REGISTRY_IMAGE:dotnet .
c45527b381def7ebfffb92bbd2c1ffff8d0d1ad4:.gitlab-ci.yml:    - buildah push --disable-compression --tls-verify=false $CI_REGISTRY_IMAGE:dotnet
c45527b381def7ebfffb92bbd2c1ffff8d0d1ad4:.gitlab-ci.yml:    - apt-get -y update && apt-get -y install buildah runc
c45527b381def7ebfffb92bbd2c1ffff8d0d1ad4:.gitlab-ci.yml:    - buildah login --tls-verify=false -u "gitlab-ci-token" -p "$CI_JOB_TOKEN" $CI_REGISTRY
c45527b381def7ebfffb92bbd2c1ffff8d0d1ad4:.gitlab-ci.yml:    - buildah bud --tls-verify=false -f Dockerfile.java -t $CI_REGISTRY_IMAGE:java .
c45527b381def7ebfffb92bbd2c1ffff8d0d1ad4:.gitlab-ci.yml:    - buildah push --disable-compression --tls-verify=false $CI_REGISTRY_IMAGE:java
43140e288a619c119f479751d5398f6012b7990a:.gitlab-ci.yml:    - apt-get -y update && apt-get -y install buildah runc
...
```

We can get the content of a specific file in a previous commit with the command:

```bash
get show COMMIT_ID:FILE_PATH
```

In this example:

```bash
git show f3c4da9ced84df7efcf39356cb6b72ea4519c215:.gitlab-ci.yml
```

## Searching in commit messages

We can search for a specific pattern in the commit messages with this command:

```bash
git log --grep=<pattern> 
```

This command lists all commits whose messages contain the search pattern, for each commit it shows:
- Commit hash
- Author details
- Commit date
- Full commit message

For example:

```
$ git log --grep=test
commit 2c2c6aeca2d442f6d8a9c9c5402b318130c7bf13 (tag: v1.0.14)
Author: Luc Juggery <luc.juggery@gmail.com>
Date:   Tue Mar 19 00:27:49 2024 +0100

    fix: test gitlab and docker registry

commit 4a64f31e410f97078be6b63b0c9f3889ac2504e2 (tag: v1.0.13)
Author: Luc Juggery <luc.juggery@gmail.com>
Date:   Tue Mar 19 00:12:12 2024 +0100

    fix: another test

commit ca2b5f2f974e7ab1fa09bb58311960292cd9a524 (tag: v1.0.12)
Author: Luc Juggery <luc.juggery@gmail.com>
Date:   Tue Mar 19 00:01:45 2024 +0100

    fix: test using DockerHub
...
```