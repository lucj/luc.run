---
title: "Copy'Em"
date: 2024-11-17
tags: ["tools", "macos"]
draft: false
---

Looking for a clipboard manager on MacOS ? You know, the kind of tool that make life easier by managing all the content you copy to the clipboard ? I recommend [Copy'Em paste for Mac OS](https://apprywhere.com/ce-mac.html#/), it's incredibly handy! I've tested some other ones but went back to this one :)

## Key features

- Clipboard history is one click away
- Support for text and image
- Paste in one click or keystroke
- Organize your items and favorites
- Keyboard shortcuts for quick access
- Search your items easily
- Identify items by names
- ... and much more

![Copy'Em](./images/copy-em/screenshot.png)
