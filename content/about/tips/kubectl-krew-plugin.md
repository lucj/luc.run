---
title: "Kubectl krew plugin"
date: 2024-11-17
tags: ["kubernetes", "tips"]
draft: false
---

[Krew](https://krew.sigs.k8s.io/) is a kubectl plugin manager. With Krew you can discover, install and manage kubectl plugins.

Install Krew following the instructions in the [official documentation](https://krew.sigs.k8s.io/docs/user-guide/setup/install/)


- Listing the plugin available

```bash
kubectl krew search
```

- Getting plugin info (we consider the plugin named *tree* in this example)

{{< callout type="note" >}}
This plugin shows sub-resources of a specified Kubernetes API object in a
tree view in the command-line
{{< /callout >}}

```bash
kubectl krew info tree
```

- Installing a plugin

```bash
kubectl krew install tree
```

- Using the plugin

Calling a plugin is done with the command ```kubectl krew PLUGIN_NAME ARGS```

```bash
kubectl krew tree deploy www
```
