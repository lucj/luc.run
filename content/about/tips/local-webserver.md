---
title: "Serving static files locally"
date: 2024-11-17
tags: ["tools", "tips"]
draft: false
---

{{< callout >}}
Need to quickly share files over your local network? Here are several simple ways to create a static file server.
{{< /callout >}}

{{< cards >}}
  {{< card 
    title="Python 3"
    icon="terminal"
    subtitle="Built-in HTTP server, no installation needed" >}}
  {{< card 
    title="Python 2"
    icon="terminal"
    subtitle="Legacy Python's SimpleHTTPServer" >}}
  {{< card 
    title="Serve"
    icon="terminal"
    subtitle="Feature-rich Node.js static server" >}}
{{< /cards >}}


## Quick Start Commands

{{< callout type="info" >}}
All these servers will serve files from your current directory. Make sure you're in the right folder before starting the server These servers are for development and local use only. Don't use them in production environments.
{{< /callout >}}

{{< tabs items="Python 3,Python 2,Serve" >}}

{{< tab "Python 3" >}}
```bash
# Starts server on port 8000
python -m http.server 8000

# Access via: http://localhost:8000
```

**Features:**
- Zero installation (Python 3 included in most systems)
- Directory listing
{{< /tab >}}

{{< tab "Python 2" >}}
```bash
# Starts server on port 8000
python -m SimpleHTTPServer 8000

# Access via: http://localhost:8000
```

**Note:** Only use if Python 3 isn't available
{{< /tab >}}

{{< tab "Serve" >}}
First, install using npm:
```bash
npm install --global serve
```

Then run:
```bash
serve  # Default port 3000
# OR
serve --port 8000  # Custom port
```
{{< /tab >}}

{{< /tabs >}}