---
title: 'About me'
disable_sidebar: true
layout: "single"
---

I'm Luc Juggery, Developer Relations Engineer @Exoscale. I'm a software engineer with over 20 years of experience in both large corporations and startups, including roles as a co-founder. I continuously expand my knowledge through side projects, online courses, and participation in technical events - as both speaker and attendee.

My interest in the container ecosystem has been a significant part of my career, during which I developed expertise in areas such as Docker, Kubernetes, cloud-native applications, DevOps, infrastructure as code, and CI/CD pipelines. I'm passionate about learning, teaching, and sharing knowledge on these technologies.

<br/>

<div style="display: flex; gap: 10px; align-items: center; justify-content: center;">
Feel free to reach out if you have any questions or would like to connect.
</div>

<div style="display: flex; gap: 10px; align-items: center; justify-content: center;">
  <!-- LinkedIn -->
  <a href="https://linkedin.com/in/ljuggery" target="_blank">
    <img src="https://cdn-icons-png.flaticon.com/512/174/174857.png" alt="LinkedIn" width="24" height="24">
  </a>
  
  <!-- Twitter -->
  <a href="https://twitter.com/lucjuggery" target="_blank">
    <img src="./images/social/x.png" alt="X" width="24" height="24">
  </a>

  <!-- BlueSky -->
  <a href="https://bsky.app/profile/luc.run" target="_blank">
    <img src="./images/social/bluesky.png" alt="BlueSky" width="24" height="24">
  </a>
  
  <!-- GitLab -->
  <a href="https://gitlab.com/lucj" target="_blank">
    <img src="https://cdn-icons-png.flaticon.com/512/5968/5968853.png" alt="GitLab" width="24" height="24">
  </a>
  
  <!-- GitHub -->
  <a href="https://github.com/lucj" target="_blank">
    <img src="https://cdn-icons-png.flaticon.com/512/733/733553.png" alt="GitHub" width="24" height="24">
  </a>
</div>

## Community involvement

**CNCF Sophia-Antipolis**

In September 2024 we decided to revive the [Sophia-Antipolis](https://www.sophia-antipolis.fr/en/) CNCF community, which was active before COVID. We aim to rebuild this great [local group](https://community.cncf.io/cloud-native-sophia-antipolis/) hosting regular meetups.

Have something to share? Consider submitting [a talk proposal](https://conference-hall.io/cloud-native-sophia-antipolis).

<div style="display: flex; align-items: center; margin-top: 20px; gap: 20px;">
  <img src="./images/meetups/20250423.png" alt="Meetup" style="width: 220px; flex-shrink: 0;">
  <div style="flex-grow: 1; text-align: left;">
    <strong>Soirée Grafana</strong> (April 23rd, 2025) - Information à venir
  </div>
</div>

<div style="display: flex; align-items: center; margin-top: 20px; gap: 20px;">
  <img src="./images/meetups/20250109.png" alt="Meetup" style="width: 220px; flex-shrink: 0;">
  <div style="flex-grow: 1; text-align: left;">
    <a href="https://community.cncf.io/events/details/cncf-cloud-native-sophia-antipolis-presents-platform-engineering-overview/" target="_blank">
      <strong>Platform Engineering Overview</strong> (January 9th, 2025)
    </a>
  </div>
</div>

<div style="display: flex; align-items: center; margin-top: 20px; gap: 20px;">
  <img src="./images/meetups/20241205.png" alt="Meetup" style="width:220px; flex-shrink: 0;">
  <div style="flex-grow: 1; text-align: left;">
    <a href="https://community.cncf.io/events/details/cncf-cloud-native-sophia-antipolis-presents-operateur-kubernetes-et-discussions-tech/" target="_blank">
      <strong>Operateur Kubernetes et discussions tech</strong> (December 5th, 2024)
    </a>
  </div>
</div>

<div style="display: flex; align-items: center; margin-top: 20px; gap: 20px;">
  <img src="./images/meetups/20241015.png" alt="Meetup" style="width: 220px; flex-shrink: 0;">
  <div style="flex-grow: 1; text-align: left;">
    <a href="https://community.cncf.io/events/details/cncf-cloud-native-sophia-antipolis-presents-gitops-et-observabilite/" target="_blank">
      <strong>GitOps & Observability</strong> (October 15th, 2024)
    </a>
  </div>
</div>

**Docker Nice Meetup** *(past)*

- Former co-organizer and regular speaker
- Led workshops on Docker fundamentals and advanced orchestration
- Topics covered: containerization, Kubernetes, cloud-native tools

**TelecomValley Open-source Community** *(past)*

- Former community organizer and technical speaker
- Conducted hands-on workshops on:
  - Docker containerization
  - Kubernetes deployment and management
  - CI/CD pipelines

## Side projects

- [Plugin to manage Helmfile with Argo CD](https://github.com/lucj/argocd-helmfile-plugin)
- [WebHooks application](https://gitlab.com/web-hook) - [https://webhooks.app](https://webhooks.app)
- [Fakely application](https://gitlab.com/fakely) - [https://fakely.app](https://fakely.app)
- [VotingApp](https://gitlab.com/voting-application) - [https://vote.votingapp.xyz](https://vote.votingapp.xyz)
- [Shape-it](https://gitlab.com/shape-it) - [https://shapes.techwhale.io/](https://shapes.techwhale.io/)
- [BTC price](https://gitlab.com/lucj/btcprice)


## Content creation

### Articles

Recent publication on [Exoscale blog](https://www.exoscale.com/syslog/):

- 2025 Feb 24 - [Sign Container Images With Cosign](https://www.exoscale.com/syslog/2025-02-24-sign-container-images-with-cosign)
- 2024 Dec 9th - [Connecting to your cluster using Dex](https://www.exoscale.com/syslog/2024-12-09-sks-oidc-dex-gitlab/)
- 2024 Nov 25th - [Conditional writes in Exoscale SOS](https://www.exoscale.com/syslog/exoscale-sos-conditional-write/)
- 2024 Nov 20th - [Connecting to your cluster using GitLab and OIDC](https://www.exoscale.com/syslog/exoscale-sks-oidc-gitlab/)
- 2024 Oct 30th - [A deep dive into Exoscale SKS internals](https://www.exoscale.com/syslog/exoscale-sks/)

Most of my previous technical blog posts were published on [Medium](https://lucjuggery.medium.com), where you'll find multiple technical articles on Docker and container orchestration I published when I was part of the Docker Captain community.

### Workshops

Overview of Exoscale offerings in this [self-paced workshop](https://practice.exoscale.com)

### Presentations

PDF listing [some Kubernetes mistakes to avoid](/kubernetes-mistakes.pdf)

### Ressources en Français

{{< cards cols="3" >}}

{{< card
title="La chaine Youtube TechWhale"
subtitle="Expliquons simplement des choses complexes" 
image="./images/youtube.jpg"
link="https://www.youtube.com/@TechWhale/videos">}}

{{< card
title="Cours d'introduction à Docker"
subtitle="" 
image="./images/udemy-docker.jpg"
link="https://www.udemy.com/course/la-plateforme-docker/?referralCode=2D28CA64EF6830F08F71">}}

{{< card
title="Cours d'introduction à Kubernetes"
subtitle=""
image="./images/udemy-k8s.jpg"
link="https://www.udemy.com/course/la_plateforme_k8s/?referralCode=8DA71FBFF795B959CCA7">}}

{{< /cards >}}

## Conferences, Talks, and Workshops

- Assisted [Acorn Labs](https://acorn.io) booth at KubeCon EU Paris (2024)
- [Pipeline CI/CD dans un environnement multi-cluster avec KubeSphere / DEVOPS D-DAY #6 (2021)
](https://www.youtube.com/watch?v=ZROUAF_1Z9w)
- Assisted Lee Calcote during the "Using Istio Workshop" at DockerCon EU (2018)
- [Orchestration d'une application micro-services avec containerPilot / DEVOPS D-DAY #3](https://www.youtube.com/watch?v=yzK9IkJyu2s) (2017)

### Quick technical Tips & Tricks

<br/>

{{< tips-list >}}