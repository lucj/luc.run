---
title: 'Ghost'
weight: 5
---

Ghost is an application used to create and publish blog articles.

The following command allows installing Ghost on Kubernetes: 

```
kubectl apply -f https://luc.run/ghost.yaml
```

The web application is exposed through a NodePort Service on port 31005

![Ghost](../images/ghost.png)