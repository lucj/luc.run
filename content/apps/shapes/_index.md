---
title: 'Shapes'
weight: 6
---

*shapes* is a simple Demo Application which only displays a colored geometrical shape (circle, triangle or square).

![Shapes](../images/shapes.png)

- Code of the [Python flask app](https://gitlab.com/shape-it/www)
- Images distributed from the [DockerHub](https://hub.docker.com/repository/docker/lucj/shapes/general)
- Helm Chart also distributed from the [DockerHub](https://hub.docker.com/repository/docker/lucj/shapeit/general)

It can be installed in a Kubernetes cluster using the following command:
```
helm upgrade --install shapes oci://registry-1.docker.io/lucj/shapeit -n shapes --create-namespace
```

Available versions are listed in the dedicated [DockerHub repository](https://hub.docker.com/repository/docker/lucj/shapeit/tags)

{{< callout type="info" >}}
You can view a live version of this application at https://shapes.techwhale.io
{{< /callout >}}

Feel free to explore the [application code](https://gitlab.com/shape-it)