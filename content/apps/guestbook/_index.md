---
title: 'Guestbook'
weight: 4
---

This is a basic application to enter and display comments.

It can be installed on Kubernetes using the following command:

```
kubectl apply -f https://luc.run/guestbook.yaml
```

The application is launched in the namespace *guestbook* and the frontend exposed through a NodePort Service on port *31000*.

![Guestbook](../images/guestbook.png)