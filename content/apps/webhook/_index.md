---
title: 'Webhook'
weight: 9
---

This is a simple application providing always-on webhooks for tests and demo purposes

![Webhook](../images/webhook.png)

It can be installed using the following command:

```bash
helm install webhooks oci://registry-1.docker.io/lucj/webhooks --version v1.0.9
```

{{< callout type="info" >}}
You can view a live version of this application at https://webhooks.app
{{< /callout >}}

Feel free to explore the [application code](https://gitlab.com/web-hook)