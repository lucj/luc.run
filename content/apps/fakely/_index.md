---
title: 'Fakely'
weight: 8
---

Simple web application used to simulate data sending for testing and demo purposes.

![Fakely](../images/fakely.png)

It can be installed using the following command:

```bash
helm pull oci://registry-1.docker.io/lucj/fakely --version v1.0.4
```

{{< callout type="info" >}}
You can view a live version of this application at https://fakely.app
{{< /callout >}}

Feel free to explore the [application code](https://gitlab.com/fakely)