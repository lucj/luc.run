---
title: 'TICK stack'
weight: 2
---

This stack is primarily used for monitoring purposes and is composed of the following components:  

- Telegraf: data ingestion
- InfluxDB: data storage (time series)
- Chronograf: data visualization
- Kapacitor: real time data analysis

![TICK](../images/tick.png)

The stack can be installed using the following command:

```
$ kubectl apply -f https://luc.run/tick.yaml
```

All the components are created in the namespace *tick* and are exposed through NodePort Services on the following ports:  

| Component  | NodePort |
| ---------  | -------- |
| Telegraf   | 31601    |
| Chronograf | 31602    |
| Kapacitor  | 31603    |

If an Ingress controller is running:  

- Telegraf is available from *telegraf.tick.com*
- Chronograf is available from *chronograf.tick.com*

{{< callout type="warning" >}}
Domain names need to be resolved with the Ingress Controller external IP first
{{< /callout >}}