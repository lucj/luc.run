---
title: 'Elastic stack'
weight: 1
---

Elastic is a well known stack used for log managements. It is composed of several components:  

- Logstash: log ingestion
- Elasticsearch: log indexing
- Kibana: log visualization

![Elastic](../images/elastic.jpeg)

This stack can be deployed on an existing cluster as follows:

```
kubectl apply -f https://luc.run/elastic.yaml
```

All the components are created in the namespace *elastic* and are exposed through NodePort Services on the following ports:

| Component  | NodePort |
| ---------  | -------- |
| Logstash   | 31500    |
| Kibana     | 31501    |

If an Ingress controller is running:  

- Logstach is available from logstash.elk.io
- Kibana is available from kibana.elk.io

{{< callout type="warning" >}}
Domain names need to be resolved with the Ingress Controller external IP first.
{{< /callout >}}
