---
title: 'Sample applications'
disable_sidebar: true
layout: "single"
---
![Votingapp](./images/voting.png)


This section presents a collection of applications designed for learning and demonstrating Kubernetes concepts. You can deploy them easily using either YAML manifests or Helm charts.

{{< cards cols="3" >}}

{{< card
title="Elastic Stack"
subtitle="used for log management"
tag="yaml"
tagType="info"
icon="tag"
link="./elastic/">}}

{{< card
title="TICK stack"
subtitle="used for metrics management"
tag="yaml"
tagType="info"
icon="tag"
link="./tick/">}}

{{< card
title="Voting Application"
subtitle="demo application to vote and get the results"
tag="yaml"
tagType="info"
icon="tag"
link="./votingapp/">}}

{{< card
title="Guestbook"
subtitle="sample application managing comments"
tag="yaml"
tagType="info"
icon="tag"
link="./guestbook/">}}

{{< card
title="Ghost"
subtitle="used to write and publish blog articles"
tag="yaml"
tagType="info"
icon="tag"
link="./ghost/">}}

{{< card
title="Webhooks"
subtitle="provides a webhook for testing purposes"
tag="helm"
tagType="error"
icon="tag"
link="./webhook/">}}

{{< card
title="Fakely"
subtitle="send dummy JSON payloads to specific URL"
tag="helm"
tagType="error"
icon="tag"
link="./fakely/">}}

{{< card
title="ShapeIt"
subtitle="toy application consisting of a very simple web frontend"
tag="helm"
tagType="error"
icon="tag"
link="./shapes/">}}

{{< /cards >}}