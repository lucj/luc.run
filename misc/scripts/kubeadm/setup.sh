# Create a VM on exoscale
NAME="$1"
ZONE="de-fra-1"
TYPE="medium"
exo compute instance create $NAME \
    --zone $ZONE \
    --disk-size 50 \
    --instance-type "standard.$TYPE" \
    --template "Linux Ubuntu 20.04 LTS 64-bit" \
    --label usage=training \
    --security-group default \
    --ssh-key "training"

# Get VM's IP address
export VM_IP=$(exo compute instance show $NAME -O json | jq -r .ip_address)

# Copy cluster installation script on VM
scp -o "StrictHostKeyChecking no" cluster-setup.sh root@${VM_IP}:/tmp/cluster-setup.sh

# Run cluster setup
ssh root@${VM_IP} -- "/tmp/cluster-setup.sh -l ${VM_IP} -p 6443"

# Retrieve kubeconfig
scp root@${VM_IP}:/root/kubeconfig.cfg ${NAME}.kubeconfig

# Modify kubeconfig so user/cluster/context names are unique
sed -i '' "s/kubernetes/kubernetes-$NAME/" ${NAME}.kubeconfig
