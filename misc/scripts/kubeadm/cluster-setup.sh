# Usage
function usage {
  echo -e "$1\n"
  echo "This script creates a single master / multiple workers K8s cluster using *kubeadm*"
  echo "usage: cluster-setup.sh -l LOAD_BALANCER_HOST -p LOAD_BALANCER_PORT [options]"
  exit 1
}

# Install prerequisites
# - Docker
# - Multipass
# - jq
function install_prerequisites {
  curl -sSL get.docker.com | sh
  snap install multipass
  snap install jq
  snap install kubectl --classic
}

# Launch master and worker VM
function launch_vms {
  multipass launch -n master
  multipass launch -n worker

  # Get IP of master
  MASTER_IP=$(multipass info master --format json | jq -r .info.master.ipv4[0])
}

# Configure Load balancer
function configure_load_balancer {
mkdir haproxy 2>/dev/null
cat<<EOF > haproxy/haproxy.cfg
############## Default #############
defaults
        mode http
        timeout connect 5000ms
        timeout client 50000ms
        timeout server 50000ms

############## Stats #############
listen stats
  bind    *:9000
  mode    http
  stats   enable
  stats   hide-version
  stats   uri       /stats
  stats   refresh   30s
  stats   realm     Haproxy\ Statistics
  stats   auth      Admin:Password

############## Configure HAProxy Secure Frontend #############
frontend k8s-api-https-proxy
    bind :443
    mode tcp
    tcp-request inspect-delay 5s
    tcp-request content accept if { req.ssl_hello_type 1 }
    default_backend k8s-api-https

############## Configure HAProxy SecureBackend #############
backend k8s-api-https
    balance roundrobin
    mode tcp
    option tcp-check
    default-server inter 10s downinter 5s rise 2 fall 2 slowstart 60s maxconn 250 maxqueue 256 weight 100
    server k8s-api-1 ${MASTER_IP}:6443 check
EOF
}

# Run HAProxy as Load balancer
function launch_load_balancer {
  docker run -d --net host -v $PWD/haproxy:/usr/local/etc/haproxy:ro haproxy:2.5.4-alpine
}

# Add dependencies (container runtime, packages, ...)
function install_dependencies {
  multipass exec master -- /bin/bash -c "curl https://luc.run/kubeadm/latest/master.sh | sh"
  multipass exec worker -- /bin/bash -c "curl https://luc.run/kubeadm/latest/worker.sh | sh"
}

# Init the cluster
function init_cluster {
  multipass exec master -- sudo kubeadm init --ignore-preflight-errors=NumCPU,Mem --control-plane-endpoint ${LOAD_BALANCER_HOST}:${LOAD_BALANCER_PORT} --upload-certs || usage "Error during the cluster initialisation"
}

# Add worker node
function add_worker {
  echo "-> add worker node"
  # Get join token from master
  JOIN=$(multipass exec master -- /bin/bash -c "sudo kubeadm token create --print-join-command 2>/dev/null")
  JOIN=$(echo $JOIN | tr -d '\r')

  # Use join token to add worker
  multipass exec worker -- sudo $JOIN || usage "Error while joining worker"
}

#  Install Network Plugin
function install_network_plugin {
  echo "-> installing WeaveNet network plugin"
  VERS=$(multipass exec master -- sudo kubectl --kubeconfig=/etc/kubernetes/admin.conf version | base64 | tr -d '\n')
  multipass exec master -- sudo kubectl apply --kubeconfig=/etc/kubernetes/admin.conf -f "https://cloud.weave.works/k8s/net?k8s-version=$VERS" || usage "Error while installing network plugin"
}

function get_kubeconfig {
  echo "-> getting kubeconfig file"
  multipass exec master -- sudo cat /etc/kubernetes/admin.conf > kubeconfig.cfg || usage "Error retreiving kubeconfig"
}

# Default
CPU=1
MEM="1G"
DISK="5G"
LOAD_BALANCER_HOST=
LOAD_BALANCER_PORT=

# Manage parameters
while getopts "c:m:d:h:l:p:" opt; do
  case $opt in
    c)
      CPU=$OPTARG
      ;;
    m)
      MEM=$OPTARG
      ;;
    d)
      DISK=$OPTARG
      ;;
    l)
      LOAD_BALANCER_HOST=$OPTARG 
      ;;
    p)
      LOAD_BALANCER_PORT=$OPTARG
      ;;
    h)
      usage
      ;;
    *)
      usage
      ;;
    :)
      echo -e "Option -$OPTARG requires an argument.\n" >&2
      help
      ;;
  esac
done

# Make sure load balancer host and port are provided
if [ -z "${LOAD_BALANCER_HOST}" ]; then
  usage "load balancer must be provided"
fi
LOAD_BALANCER_PORT=${LOAD_BALANCER_PORT:-6443}

install_prerequisites
launch_vms
configure_load_balancer
launch_load_balancer
install_dependencies
init_cluster
add_worker
install_network_plugin
get_kubeconfig
